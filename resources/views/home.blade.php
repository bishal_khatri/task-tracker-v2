@extends('layouts.app')

@section('page-title')
	Dashboard
@endsection

@section('css')
	<style>

		.blink_button {
			background-color: #004A7F;
			-webkit-border-radius: 10px;
			border-radius: 10px;
			border: none;
			color: #FFFFFF;
			cursor: pointer;
			display: inline-block;
			/*font-family: Arial;*/
			font-size: 13px;
			padding: 4px 10px 5px 10px;
			text-align: center;
			text-decoration: none;
			-webkit-animation: glowing 1500ms infinite;
			-moz-animation: glowing 1500ms infinite;
			-o-animation: glowing 1500ms infinite;
			animation: glowing 1500ms infinite;
		}

		@keyframes glowing {
			0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
			50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
			100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
		}
	</style>
@endsection

@section('content')
	<div class="white-box-title">
		<div class="row">
			<div class="col-lg-3 col-md-6">
				<a href="{{ route('home','type=0') }}">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-tasks fa-5x"></i>
								</div>

								<div class="col-xs-9 text-right">
									<div class="huge">
										@if(isset($users))
											{{ count($users->tasks->where("status", 0)) }}
										@elseif(isset($new))
											{{ $new }}
										@else
											{{ count($tasks->where("status", 0)) }}

										@endif
									</div>
									<div>New Task</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-3 col-md-6">
				<a href="{{ route('home','type=1') }}">
					<div class="panel panel-yellow">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-folder-open fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">
										@if(isset($users))
											{{ count($users->tasks->where("status", 1)) }}
										@elseif(isset($opened))
											{{ $opened }}
										@else
											{{ count($tasks->where("status", 1)) }}

										@endif
									</div>
									<div>Opened Task</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-3 col-md-6">
				<a href="{{ route('home','type=2') }}">
					<div class="panel panel-red">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-pause fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">
										@if(isset($users))
											{{ count($users->tasks->where("status", 2)) }}
										@elseif(isset($pending))
											{{ $pending }}
										@else
											{{ count($tasks->where("status", 2)) }}

										@endif
									</div>

									<div>Pending Task</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-3 col-md-6">
				<a href="{{ route('home','type=3') }}">
					<div class="panel panel-green">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-check fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">
										@if(isset($users))
											{{ count($users->tasks->where("status", 3)) }}
										@elseif(isset($completed))
											{{ $completed }}
										@else
											{{ count($tasks->where("status", 3)) }}
										@endif
									</div>
									<div>Completed Task</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<!-- /.row -->
	<?php
	if(isset($users))
	{
		$count = $users->tasks->count()>0;
		$user = $users->tasks->sortBy('name');
	}
	else{
		$count = $tasks->count()>0;
		$user = $tasks->sortBy('name');
	}
	?>
	<div class="white-box">
		<div class="row" style="min-height: 65vh;">
			<div class="col-lg-12">
				<div class="main-box clearfix">
					<div class="table-responsive">
						<table class="table table-striped table-bordered myTable">
							<thead>
							<tr>
								<th style="width: 70px;">Task ID</th>
								<th>Task Title</th>
								<th>Assigned Date:</th>
								<th>Deadline</th>
								<th>Task Status</th>
								<th>Priority</th>
								<th>Type</th>
								<th>Assigned By</th>
								<th>Client Name</th>
								<th>Client Number</th>
							</tr>
							</thead>
							<tbody>
							@if ($count>0)
								@foreach ($user->sortBy('status',0) as $task)
									<tr class="odd gradeX">
										<td onclick="trclick({{$task->id}})">{{ $loop->iteration}}</td>
										<td onclick="trclick({{$task->id}})">
											@if(strlen($task->name) >30)
											{{substr($task->name,0,50)}}
											</br>
											{{substr($task->name,50,strlen($task->name))}}

											@else
												{{$task->name}}
											@endif
										</td>
										<td onclick="trclick({{$task->id}})">{{ date('Y-m-d', strtotime($task->created_at)) }}</td>
										<td onclick="trclick({{$task->id}})">{{$task->deadline}} <br>
											<?php
											$datetime1 = new DateTime($task->deadline);
											$datetime2 = new DateTime(date('Y-m-d'));
											$difference = $datetime1->diff($datetime2);
											if($difference->days <2){
												echo "<span style='color:darkred;'>Expiring Soon </span>";
											}
											?>
										</td>
										<td class="center" onclick="trclick({{$task->id}})" >
											@if($task->status==0)
												<p class="list-group-item-text">
													<span class="label label-primary blink_button" >New</span>
												</p>
											@elseif($task->status==1)
												<p class="list-group-item-text">
													<span class="label label-warning">Opened</span>
												</p>
											@elseif($task->status==2)
												<p class="list-group-item-text">
													<span class="label label-danger">Pending</span>
												</p>
											@elseif($task->status==3)
												<p class="list-group-item-text">
													<span class="label label-success">Completed</span>
												</p>
											@endif
										</td>
										{{--<td onclick="trclick({{$task->id}})">--}}
										{{--<a href="{{ route('task.view',$task->id) }}" class="btn btn-link btn-sm btn-primary">View</a>--}}
										{{--</td>--}}
										<td onclick="trclick({{$task->id}})">
											@if($task->priority == 1)
												<span class="label label-default">Normal</span>
											@elseif($task->priority == 2)
												<span class="label label-warning">Important</span>
											@else
												<span class="label label-danger">Urgent</span>
											@endif
										</td>

										<td onclick="trclick({{$task->id}})">
											@if(!empty($task->task_type))
												{{ucfirst($task->task_type)}}
											@else
												No Task Type Set
											@endif
										</td>
										{{--Assigned by--}}
										<td onclick="trclick({{$task->id}})">
											{{ ucfirst($task->full_name) }}
										</td>

										<td onclick="trclick({{$task->id}})">
											{{ ucfirst($task->client_name) }}
										</td>

										<td onclick="trclick({{$task->id}})">
											{{ ucfirst($task->number) }}
										</td>

									</tr>
								@endforeach
							@else
								<div class="alert alert-warning" role="alert"><i class="fa fa-info-circle fa-fw"></i>No task has been assigned</div>
							@endif
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>



	@include('homemodal')
@endsection
@section('script')
	<script>
		$(document).ready( function () {
			// $('#myTable').DataTable();
			$('.myTable').dataTable( {
				"pageLength": 50,
				"lengthMenu": [ 10, 25, 50, 75, 100 ]
			} );
		} );

		function trclick(id){
			//server address
			// location.replace("http://localhost:83/task/task/view/"+id)
			//public  locally
			location.replace("http://117.121.237.226:83/task/task/view/"+id);

		};

	</script>
	<script type="text/javascript">
		$('#statusModal').on('show.bs.modal', function (event) {
			let button = $(event.relatedTarget) // Button that triggered the modal
			let task_status = button.data('task_status')
			let task_id = button.data('task_id')

			let modal = $(this)
			modal.find('.modal-body #task_status').val(task_status)
			modal.find('.modal-body #task_id').val(task_id)

			//    if status is 3 let user to upload file too.
			if(task_status == 3){
				modal.find('.modal-body #file_upload').show()
			}
			else{
				modal.find('.modal-body #file_upload').hide()
			}
		})

		$(window).on('load',function(){
			$('#homemodal').modal('show');
		});
	</script>
@endsection

