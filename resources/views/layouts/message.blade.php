@if(Session::has('success'))
	<div class="alert alert-success" role="alert">Success :  {{ Session::get('success')}}</div>
@elseif (Session::has('error'))
	<div class="alert alert-danger" role="alert">Fail : {{ Session::get('error')}}</div>
@endif

{{--@if(count($errors) > 0)--}}
	{{--<div class="alert alert-danger" role="alert">--}}
		{{--@foreach($errors->all() as $error)--}}
			{{--<small>{{ $error }}</small>--}}
		{{--@endforeach--}}
	{{--</div>--}}
{{--@endif--}}