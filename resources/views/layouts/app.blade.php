<!DOCTYPE html>
<html lang="en">
@include('layouts.partials._head')
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}">Task Management</a>
            </div>
           @include('layouts.partials._nav')
            @include('layouts.partials._sidebar')
        </nav>
        
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

    </div>
@include('layouts.partials._footer')
