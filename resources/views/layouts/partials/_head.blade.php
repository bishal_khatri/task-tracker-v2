<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Task Management - @yield('page-title')</title>
    <link rel="icon" href="{!! asset('/public/icon/tech.gif') !!}"/>
    <link href="{{ asset('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/dist/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">

    <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/global/plugins/datatables/media/css/jquery.dataTables.min.css') }}">
    
    <style>
        .sidebar {
            z-index: 1;
            position: absolute;
            width: 235px;
            margin-top: 51px;
        }
        .page-content-wrapper .page-content {
	        margin-left: 235px;
	        margin-top: 0px;
	        min-height: 90vh;
	        /*padding: 25px 20px 10px 20px;*/
        }
        .white-box {
	        margin-top: auto;
	        margin-left: auto;
	        margin-right: auto;
	        margin-bottom: 10px;
	        background: rgba(255,255,255,0.9);
	        padding-left: 20px;
	        padding-right: 20px;
	        padding-top: 20px;
	        padding-bottom: 0px;
	        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.15);
	        -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.15);
	        box-shadow: 0 1px 2px rgba(0,0,0,0.15);
	        -webkit-border-radius: 5px;
	        -moz-border-radius: 5px;
	        -ms-border-radius: 5px;
	        -o-border-radius: 5px;
	        border-radius: 5px;
	        /*border: 1px solid #23282c;*/
        }
        .white-box-title {
	        margin-left: auto;
	        margin-right: auto;
	        background: rgba(255,255,255,0.9);
	        /*padding: 15px 10px 0px 10px;*/
	        padding-left: 20px;
	        padding-right: 20px;
	        padding-top: 20px;
	        padding-bottom: 0px;
	        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.15);
	        -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.15);
	        box-shadow: 0 1px 2px rgba(0,0,0,0.15);
	        -webkit-border-radius: 5px;
	        -moz-border-radius: 5px;
	        -ms-border-radius: 5px;
	        -o-border-radius: 5px;
	        border-radius: 5px;
	        /*border: 1px solid #23282c;*/
        }
        .page-content{
	        background-color: #f4f5f4 !important;
        }
	    .form-actions{
		    padding-bottom: 20px;
	    }
    </style>
    @yield('css')

</head>