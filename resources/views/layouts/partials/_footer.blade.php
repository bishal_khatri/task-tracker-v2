<!-- BEGIN FOOTER -->
<div class="page-footer" style="background-color: #c5c5c5;margin-bottom:0px;">
    <div class="page-footer-inner col-md-12 text-center">
        <p><?= date('Y') ?> &copy; Task Management System developed by <strong><a href="http://ramlaxmangroup.com/">Technology Sales Pvt. Ltd.</a></strong></p>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<script src="{{ asset('public/assets/global/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('public/metisMenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('public/dist/js/sb-admin-2.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'assets/global/plugins/jquery-ui/jquery-ui.min.js') }}"></script><script src="{{ asset('public/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>

@yield('script')
<script>



</script>
</body>

</html>
