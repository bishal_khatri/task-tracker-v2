<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">

                {{-- profile prefiew --}}
                <div class="row"  >
                    <div class="profile-header-container">
                        <img id="profile image"  @if(Auth::user()->profile_image == null)
                                                        src="{{asset(STATIC_DIR.'images/default.jpg')}} "
                                                 @else
                                                         src="{{asset(STATIC_DIR.'storage/'.Auth::user()->profile_image)}}"
                                                @endif
                             style="max-height: 90px; max-width: 150px"   />

                        <!-- badge -->
                        <div class="rank-label-container">
                            <span class="label label-default rank-label">{{ Auth::user()->full_name }}</span>
                        </div>
                    </div>
                </div>
                {{-- end profile preview --}}

            </li>

            <li>
                <a href="{{ route('home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>

            @if (Auth::user()->isAdmin()==1)

            <li>
                <a href="{{ route('profile.view',Auth::user()->id) }}"><i class="glyphicon glyphicon-user"></i> My profile</a>
            </li>

                <li>
                    <a href="#"><i class="glyphicon glyphicon-user"></i> Staff management <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('register') }}">
                                <i class="fa fa-plus"></i>Register Staff
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('staff.show') }}">
                                <i class="fa fa-list"></i> View Staff</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            @endif

            @if (Auth::user()->user_type==1)
            <li>
                <a href="#"><i class="glyphicon glyphicon-tasks"></i> Task Management<span class="fa arrow"></span></a>

                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('task.client-retrieve') }}"><i class="glyphicon glyphicon-user"></i> Clients</a>
                    </li>

                    <li>
                        <a href="{{ route('task.create') }}"><i class="glyphicon glyphicon-pencil"></i> Create new task</a>
                    </li>

                    <li>
                        <a href="{{ route('task.show') }}"><i class="glyphicon glyphicon-eye-open"></i> View all task</a>
                    </li>

                    <li>
                        <a href="{{ route('task.assign') }}"><i class="glyphicon glyphicon-share"></i> Assign task</a>
                    </li>

                    <li>
                        <a href="{{ route('task.show_completed_task') }}"><i class="glyphicon glyphicon-check"></i> Completed task</a>
                    </li>

                </ul>
            </li>
            @endif

            @if(Auth::user()->user_type == 1)
                <li>
                    <a href="#"><i class="glyphicon glyphicon-phone-alt"></i> Telephone Directory<span class="fa arrow"></span></a>

                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('telephone.register_telephone') }}"><i class="glyphicon glyphicon-pencil"></i> Create New Telephone</a>
                        </li>

                        @endif

                        <li>
                            <a href="{{route('telephone.index')}}"><i class="	glyphicon glyphicon-earphone"></i> Show Telephone List</a>
                        </li>

                    </ul>
                </li>

                @if (Auth::user()->user_type==1)
                    <li>
                        <a href="#"><i class="glyphicon glyphicon-tasks"></i> Leave Management<span class="fa arrow"></span></a>

                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ route('leave.register_leave') }}"><i class="glyphicon glyphicon-pencil"></i> Permission Form</a>
                            </li>

                            <li>
                                <a href="{{ route('leave.list') }}"><i class="glyphicon glyphicon-eye-open"></i> View all Leave</a>
                            </li>

                        </ul>
                    </li>
                @endif


            {{--<li>
                <a href="{{ route('home') }}"><i class="glyphicon glyphicon-cog"></i> Service & Maintenance Form</a>
            </li>--}}



                {{--<li>
                    <a href="#"><i class="glyphicon glyphicon-open-file"></i> Upload Files</a>
                </li>--}}

                {{--<li>
                    <a href="{{ route('staff.role') }}"><i class="fa fa-edit fa-fw"></i> Change staff role</a>
                </li>--}}

                {{--<li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Department <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('department.home') }}"> View department</a>
                        </li>
                        <li>
                            <a href="{{ route('department.create') }}"> Add department</a>
                        </li>

                    </ul>
                    <!-- /.nav-second-level -->
                </li>--}}
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>