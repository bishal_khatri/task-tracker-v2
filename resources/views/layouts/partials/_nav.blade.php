<ul class="nav navbar-top-links navbar-right">

    <!-- /.dropdown -->
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="{{ route('profile.view',Auth::user()->id) }}"><i class="fa fa-user fa-fw"></i> My Profile</a>
            </li>
            {{--<li><a href="{{ route('staff.edit', Auth::user()->id) }}"><i class="fa fa-gear fa-fw"></i> Settings</a>--}}
            {{--</li>--}}
            {{--<li class="divider"></li>--}}
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i>  Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>