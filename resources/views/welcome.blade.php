<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Technology Sales</title>
<link rel="icon" href="{!! asset('/public/icon/tech.gif') !!}"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                background-image:url({{ asset("public/icon/work.jpeg") }});

                background-repeat:no-repeat;

                background-size:cover;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 150px;
                text-align: center;
                font-weight: bold;
                color: black;
            }

            .sub-title {
                font-size: 30px;
                text-align: center;
                font-weight: bold;
                color: black;
            }

            .links > a {
                background-color: transparent;
                color: black;
                padding: 15px;
                font-size: 15px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                border: 1px solid darkgreen;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body >
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>

                    @endif
                </div>
            @endif

            <div class="content" style="background-color: whitesmoke;opacity: 0.5; padding: 20px;">
                <div class="title m-b-md">
                    Technology Sales
                </div>

                <div class="links sub-title">
                   <h3>Task Panel</h3>
                </div>
            </div>
        </div>
    </body>
</html>
