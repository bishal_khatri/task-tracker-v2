@extends('layouts.app')

@section('content')
		<br>
	
<div class="container">
  <div class="col-lg-6-offset-4">
 	 <div class="col-lg-6">
  	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Staff Details</h3>
	  </div>
	  <div class="panel-body">
	   
	    	
	    	<strong>Full Name : </strong>{{ $staff->full_name }}
	    	<hr>
	    	<strong>Contact Number : </strong>{{ $staff->contact }}
	    	<hr>
	    	<strong>Joined Date : </strong>{{ $staff->join_date }}
	    	<hr>
	    	<strong>Department : </strong>{{ $staff->department }}
	    	<hr>
	    	<strong>User Name : </strong>{{ $staff->user_name }}
	    	<hr>
            <strong>Permanent Address : </strong>{{ $staff->permanent_address }}
            <hr>
            <strong>Temporary Address : </strong>{{ $staff->temporary_address }}
            <hr>
            <strong>Social Media link </strong>  <div class="col-md-12"> 
                    <ul class="social-network social-circle">
                       @if (isset($staff->facebook_link))
                            <li><a href="{{ $staff->facebook_link }}" target="_self" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                       @endif
                        @if (isset($staff->twitter_link))
                            <li><a href="#" target="_self" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        @endif
                        @if (isset($staff->google_link))
                            <li><a href="google.com" target="_blank" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                        @endif
                        @if (isset($staff->linkedin_link))
                            <li><a href="linkedin.com" target="_blank" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        @endif
                    </ul>               
                </div>
            <hr>
		    	
	    
	    </div>
	    </div>
	    </div>
	    <div class="col-lg-6">
                
                 <div class="media">
                        <div align="center">
                            <img class="thumbnail img-responsive"
                                 @if($staff->profile_image == null)
                                 src="{{ asset(STATIC_DIR.'images/default.jpg') }}"
                                 @else
                                 src="{{ asset(STATIC_DIR.'storage/'.$staff->profile_image) }}"
                                 @endif
                                 width="300px" height="300px"
                            >
                        </div>
                       
                    </div>

	    </div>

	    @if (Auth::user()->isAdmin()==1)

	    <div class="col-lg-12">
                         <div class="col-lg-12">
                        <h2>Task Assigned to {{ $staff->full_name }}</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Task ID</th>
                                        <th>Title</th>
                                        <th>Created At</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($assigned as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->created_at }}</td>
                                         <td class="center">
                                            @if($data->status==0)
                                                <p class="list-group-item-text">
                                                  <span class="label label-success">New</span>
                                                </p>
                                            @elseif($data->status==1)
                                                <p class="list-group-item-text">
                                                  <span class="label label-success">Opened</span>
                                                </p>
                                            @elseif($data->status==2)
                                                <p class="list-group-item-text">
                                                  <span class="label label-danger">Pending</span>
                                                </p>
                                            @elseif($data->status==3)
                                                <p class="list-group-item-text">
                                                  <span class="label label-success">Completed</span>
                                                </p>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('task.view',$data->id) }}" class="btn btn-default">View</a>
                                            <a href="{{ route('task.edit',$data->id) }}" class="btn btn-default">Edit</a>
                                           {{--   <form action="{{ route('task.delete',$data->id) }}" method="post">
                                             {{ csrf_field() }}
                                                    <button onclick="myFunction()" class="btn btn-danger btn-block">Delete</button>
                                             
                                            </form> --}}
                                            {{--<a href="{{ route('task.delete',$data->id) }}" class="btn btn-default">Delete </a>--}}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                         
                            <div>
                            	<strong>Showing {{ $assigned->firstItem() }} to {{ $assigned->lastitem() }} of {{ $assigned->total() }} entries </strong>
                            </div>
                            <div align="center">
                            	 {{ $assigned->links() }}
                            </div>
                      
                        </div>
                    </div>
                    </div>
                    @endif

	  </div>
	</div>

@endsection