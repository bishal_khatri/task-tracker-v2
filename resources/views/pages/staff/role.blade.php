@extends('layouts.app')

@section('content')
  <!-- Page Heading -->
           <br>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <div class="alert-danger"><strong>Type '1' to give user Administrator previllage Or '0' to give User previllage</strong></div>
           <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>@foreach ($staff as $data)
                    <tr>
                    
                        @if ($data->full_name!='Administrator')
                        <form class="form-horizontal" method="post" action="{{ route('staff.role',$data->id) }}">
                            {{ csrf_field() }} 
                            <td>{{ $data->full_name }}</td>
                            <td>
                                <input type="number" name="user_type" value="{{ $data->user_type }}" class="form-control">
                            </td>
                            <td>
                                <input type="submit"  value="Change" class="btn btn-default">
                            </td>
                        </form>
                            @endif
                       
                    </tr> @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
