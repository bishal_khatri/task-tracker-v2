@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
			<div class="panel panel-default" >
				<div class="panel-body" style="min-height: 45vh">
					<div class="media" style="margin-top: 15px;">
						<div align="center">
							<img class="thumbnail img-responsive" src="
							     @if($profile->profile_image == null)
							{{ asset(STATIC_DIR.'images/default.jpg') }}
							@else
							{{ asset(STATIC_DIR.'storage/'.$profile->profile_image) }}
							@endif " width="300px" height="300px">
						</div>
						<div class="media-body">
							<hr>
							<p><strong>Joined date: </strong> {{ $profile->join_date ?? '' }}</p>
							<hr>
							<p><strong>Contact Number: </strong> {{ $profile->contact }}</p>
							<hr>
							<p><strong>Permanent Address: </strong> {{ $profile->permanent_address }}</p>
							<hr>
							<p><strong>Temporary Address: </strong> {{ $profile->temporary_address }}</p>
							<hr>
							<p><strong>Email Address: </strong> {{ $profile->email }}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-white post panel-shadow" >
						<div class="panel-body">
							<h1 class="panel-title pull-left" style="font-size:30px;">{{ $profile->full_name }}</h1>
							<a class="btn btn-link btn-sm" href="{{ route('password.edit') }}">Change password</a>
							<a class="btn btn-link btn-sm" href="{{ route('staff.edit', Auth::user()->id) }}"> Edit profile</a>
							<i class="fa fa-gear fa-fw"></i>
							<br><br>
							<strong><small>({{ $profile->user_name }})</small>  </strong>
							<br><hr>
							
							<div class="col-md-12">
								<ul class="social-network social-circle">
									<li><a href="{{ $profile->facebook_link }}" target="_blank" class="icoFacebook" style="padding-top: 10px;" title="Facebook"><i class="fa fa-facebook"></i></a></li>
									<li><a href="{{ $profile->google_link }}" target="_blank" class="icoTwitter" title="Twitter" style="padding-top: 10px;"><i class="fa fa-twitter"></i></a></li>
									<li><a href="{{ $profile->twitter_link }}" target="_blank" class="icoGoogle" title="Google +" style="padding-top: 10px;"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="{{ $profile->linkedin_link }}" target="_blank" class="icoLinkedin" title="Linkedin" style="padding-top: 10px;"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="white-box">
				<div class="row" style="min-height: 44vh">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
								<tr>
									<th style="width: 70px;">Task ID</th>
									<th>Title</th>
									<th>Created At</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@if ($assigned_completed->count()>0)
									@foreach($assigned_completed as $COMPLETED)
										<tr>
											<td>{{ $COMPLETED->id }}</td>
											<td>{{ $COMPLETED->name }}</td>
											<td>{{ $COMPLETED->created_at }}</td>
											<td>
												<a href="{{ route('task.view',$COMPLETED->id) }}" class="btn btn-default btn-block">View</a>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="4" class="text-center">No items found.</td>
									</tr>
								@endif
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>

@endsection