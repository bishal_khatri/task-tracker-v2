@extends('layouts.app')

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Department Management</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
	
	<div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Departments</h3>
                            </div>
                            <div class="panel-body">

                                <div class="list-group">
                                @foreach ($depart as $data)
                                  
                                        <div class="col-lg-8">
                                        <i class="glyphicon glyphicon-file"></i> {{$data->name}}
                                        </div>

										<div class="col-lg-4">
										<a href="{{ route('department.edit',$data->id) }}" class="btn btn-default">Edit</a>
										<a href="" class="btn btn-danger">Delete</a>
										</div>
										<br><hr>
                                    
                                @endforeach
                                </div>
                               
                            </div>
                        </div>
                    </div>

@endsection