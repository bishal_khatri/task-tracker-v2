@extends('layouts.app')
@section('page-title')
    Create Task
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('task.store') }}">
                <div class="white-box">
                    <div class="row">
                        <div class="col-md-8">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Employee Name: <span style="color:red;">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="Enter Employee Name" name="title" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('deadline') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Days <span style="color:red;">*</span></label>
                                    <div class="col-md-9">
                                        {{--<input type="date" id="deadline" class="form-control" placeholder="Standard" name="deadline" value="{{ old('deadline')}}">--}}
                                        <input type="number"  id="deadline" class="form-control" placeholder="Enter Leave  Days" name="deadline" value="{{ old('deadline')}}">
                                        @if ($errors->has('deadline'))
                                            <span class="help-block">
												<strong>{{ $errors->first('deadline') }}</strong>
											</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('deadline') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Leave Date <span style="color:red;">*</span></label>
                                    <div class="col-md-9">
                                        <input type="date" id="deadline" class="form-control" placeholder="Standard" name="deadline" value="{{ old('deadline')}}">
                                        @if ($errors->has('deadline'))
                                            <span class="help-block">
												<strong>{{ $errors->first('deadline') }}</strong>
											</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Reasons for leave</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="description" rows="15" >{{old('description')}}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="white-box">
                    <div class="row">
                        <div class="col-md-9 col-md-offset-2">
                            <div class="form-actions">
                                <button type="submit" class="btn green"><i class="fa fa-check"> Submit</i></button>
                                <button type="reset" class="btn default"><i class="fa fa-trash"> Clear</i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection