
@extends('layouts.app')

@section('content')

	<h3>Comments</h3>
			<div class="default-row-spacer">
    				<div class="row">
    					<div class="col-md-12">
    						<div class="widget-area no-padding blank">
								<div class="status-upload">
									<form action="{{ route('comment.update',$comment->id) }}" method="post">
									{{ csrf_field() }}
										<textarea placeholder="Write a comment for this task?" name="comment_edit" >{{ $comment->comment }}</textarea>
										<ul>
											<li><a title="" data-toggle="modal" data-target="#myModal3" data-placement="bottom" data-original-title="Audio"><i class="fa fa-upload"></i></a></li>
										</ul>
										<button type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Update</button>
									</form>
								</div><!-- Status Upload  -->
							</div><!-- Widget Area -->
						</div>
				</div>
			</div>

@endsection