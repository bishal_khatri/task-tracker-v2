@extends('layouts.app')
@section('page-title')
    Task Listing
@stop
@section('css')
@stop

@section('content')
    <div class="white-box">
        <div class="row" style="min-height: 83vh !important;">
            <div class="col-lg-12">
                <div class="main-box clearfix">
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Assigned to</th>
                                <th>Assigned By</th>
                                <th>Created At</th>
                                <th>Status</th>
                                <th>Deadline</th>
                                <th>Task Priority</th>
                                <th>Client Name</th>
                                <th>Client Number</th>
                                <th class="col-sm-2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($task))
                                @foreach($task->sortBy('status',0) as $val)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            @if(strlen($val->name) >30)
                                            {{substr($val->name,0,50)}}
                                            </br>
                                            {{substr($val->name,50,strlen($val->name))}}

                                            @else
                                                {{$val->name}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($val->users->count() == 0)
                                                <strong>No Staffs Assigned</strong>
                                            @else
                                                @foreach($val->users as $value)
                                                    <p>{{$value->full_name}}</p>
                                                @endforeach
                                            @endif
                                        </td>
                                        {{--Assigned by--}}
                                        <td>
                                            @if(isset($val->author->full_name))
                                                <p>{{$val->author->full_name}}</p>
                                            @endif
                                            {{--@foreach($val->author as $value)--}}
                                            {{--<p>{{$value->full_name}}</p>--}}
                                            {{--@endforeach--}}
                                        </td>

                                        <td>{{ $val->created_at->diffForHumans() }}</td>

                                        <td class="center">
                                            @if($val->status==0)
                                                <p class="list-group-item-text">
                                                    <span class="label label-primary">New</span>
                                                </p>
                                            @elseif($val->status==1)
                                                <p class="list-group-item-text">
                                                    <span class="label label-warning">Opened</span>
                                                </p>
                                            @elseif($val->status==2)
                                                <p class="list-group-item-text">
                                                    <span class="label label-danger">Pending</span>
                                                </p>
                                            @elseif($val->status==3)
                                                <p class="list-group-item-text">
                                                    <span class="label label-success">Completed</span>
                                                </p>
                                            @endif
                                        </td>
                                        <td>
                                            {{$val->deadline}}
                                        </td>

                                        <td>
                                            @if($val->priority == 1)
                                                <span class="label label-default">Normal</span>
                                            @elseif($val->priority == 2)
                                                <span class="label label-warning">Important</span>
                                            @else
                                                <span class="label label-danger">Urgent</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if($val->clients != null)
                                                {{$val->clients->name}}
                                            @else
                                                No Clients Assigned
                                            @endif
                                        </td>

                                        <td>
                                            @if($val->clients != null)
                                                {{$val->clients->number}}
                                            @else
                                                No Clients Assigned
                                            @endif

                                        </td>

                                        <td style="width: 12%;" >

                                            <a href="{{ route('task.view',$val->id) }}" class="table-link" title="View details">

                                                <i class="glyphicon glyphicon-eye-open"></i>
                                            </a> &nbsp;

                                            <a href="{{ route('task.edit',$val->id) }}" class="table-link" title="Edit">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </a> &nbsp;

                                            <a href="#deleteTask" class="table-link danger delete" title="Delete Task" data-ids="{{ $val->id }}" data-toggle="modal" data-rel="delete" data-user="{{$val->name}}">
                                                <i class="glyphicon glyphicon-trash"></i>

                                            </a>

                                        </td>


                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('pages.task.modal')
@endsection

@section('script')
    <script>
        $(document).ready( function () {
            $('#myTable').dataTable( {
                "pageLength": 50,
                "lengthMenu": [ 10, 25, 50, 75, 100 ]
            } );
        } );

        $('#deleteTask').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modalfooter #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });


        $('#deleteTask').find('#confirm_yes').on('click', function () {

            //set csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();

            $.ajax({
                type: "POST",
                url: "{{ route('task.delete') }}",
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                    console.log(msg);
                    window.location.reload();
                }
            });
        });

        $('#show_message').on('click', function () {
            window.location.reload();
        });
    </script>
@endsection