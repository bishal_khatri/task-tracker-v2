@extends('layouts.app')
@section('page-title')
    Assign Task
@stop
@section('css')
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />--}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
	        <form class="form-horizontal" role="form" method="POST" action="{{ route('task.assign') }}">
		        <div class="white-box">
			        <div class="row">
				        <div class="col-md-12">
					        {{ csrf_field() }}
							<hr>
							<p>Information of Employee and Task</p>
							<hr>
					        <div class="form-body">

								<div class="form-group">
									<label class="col-md-3 control-label">Select Employees</label>
									<div class="col-md-3">
										<select class="form-control" name="task" id="select_employee" >
											<option value="0" >-- SELECT ONE --</option>
											@if (!empty($task))
												@foreach ($staff as $data)
													<option value={{ $data->id }}>{{ $data->full_name }}</option>

												@endforeach
											@endif

										</select>
									</div>
								</div>

								<div class="form-group" id="assigned_task" style="display: none;">
									<label class="col-md-3 control-label">Assigned Tasks</label>

									<div class="col-md-9" style="margin-left: 380px; margin-top:-35px;" >
										<ol id="appendTask">
										</ol>
									</div>
								</div>

					        </div>
				        </div>

						<div class="col-md-12">
							<hr>
							<p>Assign  Task To Employee</p>
							<hr>
							<div class="col-md-8">
								<div class="form-group">
									<label class="col-md-3 control-label">Select Task</label>
									<div class="col-md-9">
										<select class="form-control" name="task" id="task">
											<option value="0" >-- SELECT ONE --</option>
											@if (!empty($task))
												@foreach ($task as $data)
													<option value={{ $data->id }}>{{ $data->name }}</option>

												@endforeach
											@endif
										</select>
									</div>
								</div>

								<div class="form-group" id="assigned_employee" style="display: none;">
									<label class="col-md-3 control-label">Assigned Employee</label>

									<div class="col-md-9" style="margin-left: 250px; margin-top:-35px;" >
										<ol id="append">
										</ol>
									</div>
								</div>

							</div>

							<div class="col-md-4">
								<div class="portlet box green-meadow">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Employee
										</div>

									</div>
									<div class="portlet-body" style="overflow-y: scroll; height: 67vh;">

										<ul class="list-group">
											<li class="list-group-item">
												@foreach ($staff as $name)
													<div class="input-group form-control">
														<input type="checkbox" class="assigned_user"
															   aria-label="..." name="staff[]"  value="{{ $name->id }}" multiple="multiple" class="form-control-sm"
                                                        <?php /*echo ($name->id == 1) ?  "checked" : '' */?>>
														<strong>
															{{ $name->full_name }}
														</strong>
													</div>
													<br>

												@endforeach

											</li>

										</ul>
									</div>
								</div>
							</div>
						</div>
			        </div>
		        </div>

		       <div class="white-box">
			       <div class="row">
				       <div class="col-md-12">
					       <div class="form-actions">
						       <a href="{{ URL::previous() }}" type="button" class="btn default">Cancel</a>
						       <button type="submit" class="btn green">Assign</button>
					       </div>
				       </div>
			       </div>
		       </div>
	        </form>
        </div>
    </div>
@endsection

@section('script')
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>--}}

    <script>
        $(document).ready(function() {
            let user = $('.assigned_user').val();
            console.log(user);
            // alert(user);
        });

        $(document).ready(function (){
            $("#task").on ('change',  function( e ){
                let task_id = $('#task').val();
                $("#append").children("li").remove();


                if(task_id == 0){
                    $('#assigned_employee').hide();

                }
                else{
					//retrieving assigned user from task id.
                    $.ajax({
                        type: "GET",
                        url: "{{ route('task.getUserByTaskId') }}",
                        headers: {
                            'XCSRFToken': $('meta[name="csrftoken"]').attr('content')
                        },

                        data: "id=" + task_id,
                        success: function (msg) {
                            $('#assigned_employee').show();
                            appendlist(msg.data);
                        }
                    });
				}


            });

            $("#select_employee").on ('change',  function( e ){
                let user_id = $(this).val();
                $("#appendTask").children("li").remove();

                if(user_id == 0){
                    $('#assigned_task').hide();
                }
                else{
                    //retrieving assigned user from task id.
                    $.ajax({
                        type: "GET",
                        url: "{{ route('task.user-task') }}",
                        headers: {
                            'XCSRFToken': $('meta[name="csrftoken"]').attr('content')
                        },

                        data: "user_id=" + user_id,
                        success: function (msg) {
                            $('#assigned_task').show();
                            appendTask(msg);

                        }
                    });
				}

            });

        });


        function appendlist(data) {

            $.each(data.users, function () {
                $("#append").append("&nbsp;<li>" + this.full_name +"</li>");
                // $(".assigned_user").prop("checked",true);

            });
        }

        function appendTask(data){
            $.each(data, function (value, key) {
                $("#appendTask").append("&nbsp;<li>" + this.name +"</li>");
            });
		}


    </script>
@endsection