@extends('layouts.app')
@section('page-title')
    Edit Task
@stop
@section('content')
        <div class="row">
            <div class="col-md-8">
                <div class="white-box">
                    <h2>Client Listing</h2>
                    <hr>

                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered" id="myTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client Name</th>
                                    <th>Contact Numer</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($client as $value)
                                <tr>
                                    <td>{{$loop->iteration }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->number }}</td>
                                    <td> {{ $value->latitude }}</td>
                                    <td>{{ $value->longitude }}</td>
                                    <td> {{ date('Y-m-d', strtotime($value->created_at)) }}</td>
                                    <td>
                                        <button class="show_client_edit" value="{{$value->id}}"><i class="fa fa-pencil"> </i></button>


                                        <a href="#deleteClient" class="btn-link danger delete" title="Delete Client" data-ids="{{ $value->id }}" data-toggle="modal" data-rel="delete" data-user="{{$value->name}}">
                                            {{--<span class="fa-stack">
                                                <i class="fa fa-square fa-stack-2x"></i>
                                                <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                            </span>--}}
                                            <i class="glyphicon glyphicon-trash"></i>

                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-4" id="client_add" >
                <div class="white-box">
                    <h2>Client Add</h2>
                    <hr>
                    <form action="{{route('task.client-store')}}" method="post">
                        @csrf

                            <div class="form-group {{ $errors->has('client_name') ? ' has-error' : '' }}">
                                <label class="control-label">Client Name <span style="color:red;">*</span></label>
                                <input type="text"  class="form-control" placeholder="Enter Client Name" name="client_name" value="{{ old('client_name')}}">

                                @if ($errors->has('client_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('client_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                                <label class="control-label">Client Number <span style="color:red;">*</span></label>
                                <input type="number"  class="form-control" placeholder="Enter Client Contact Number" name="number" value="{{ old('number')}}">
                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                                <label class="control-label">Latitude</label>
                                <input type="text"  class="form-control" placeholder="Enter Client Latitude" name="latitude" value="{{ old('latitude')}}">

                                @if ($errors->has('latitude'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('latitude') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                                <label class=" control-label">Longitude</label>
                                <input type="text"  class="form-control" placeholder="Enter Client Longitude" name="longitude" value="{{ old('longitude')}}">
                                @if ($errors->has('longitude'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('longitude') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"> <i class="fa fa-check"></i> Save Client</button>
                            </div>
                             <br>

                    </form>
                </div>
            </div>
            <div class="col-md-4" id="client_edit" style="display: none;">
                <div class="white-box">
                    <div class="row">
                            <div class="col-md-4">
                                <h2>Client Edit</h2>

                            </div>

                            <div class="col-md-8" style="text-align: right;">
                                <button id="show_client_add"> <i class="fa fa-plus"></i> Add Client</button>
                            </div>
                    </div>

                    <form action="{{route('task.client-edit')}}" method="post">
                        @csrf
                        <input type="hidden" name="client_id" id="client_id">

                        <div class="form-group {{ $errors->has('client_name') ? ' has-error' : '' }}">
                            <label class="control-label">Client Name <span style="color:red;">*</span></label>
                            <input type="text" id="client_name"  class="form-control" placeholder="Enter Client Name" name="client_name" value="{{ old('client_name')}}">

                            @if ($errors->has('client_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('client_name') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label class="control-label">Client Number <span style="color:red;">*</span></label>
                            <input type="number" id="number" class="form-control" placeholder="Enter Client Contact Number" name="number" value="{{ old('number')}}">
                            @if ($errors->has('number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                            <label class="control-label">Latitude</label>
                            <input type="text" id="latitude" class="form-control" placeholder="Enter Client Latitude" name="latitude" value="{{ old('latitude')}}">

                            @if ($errors->has('latitude'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('latitude') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                            <label class=" control-label">Longitude</label>
                            <input type="text" id="longitude"  class="form-control" placeholder="Enter Client Longitude" name="longitude" value="{{ old('longitude')}}">
                            @if ($errors->has('longitude'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('longitude') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-pencil"></i> Edit Client</button>
                        </div>
                        <br>

                    </form>
                </div>
            </div>
        </div>

@include('pages.task.modal')
@endsection

@section('script')
    <script>
        $(document).ready( function () {
            $('#myTable').dataTable( {
                "pageLength": 10,
                "lengthMenu": [ 5, 10, 15, 20, 25 ]
            } );
        } );

        //edit client
        $('.show_client_edit').on('click',function(){

            $.ajax({
                type:"POST",
                url: "{{route('task.client-retrieve-id')}}",
                data: "client_id=" + $(this).val(),
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    console.log(msg);
                    $('#client_id').val(msg.id);
                    $('#client_name').val(msg.name);
                    $('#number').val(msg.number);
                    $('#latitude').val(msg.latitude);
                    $('#longitude').val(msg.longitude);


                    // $('#edit_asset_specification_form').show();
                    // $('#add_asset_specification_form').hide();
                }
            });

            $('#client_add').hide();
            $('#client_edit').show();

        });


        $('#show_client_add').on('click',function(){
           $('#client_add').show();
           $('#client_edit').hide();

        });



        //delete client data
        $('#deleteClient').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            var modal = $(this)
            modal.find('#hidden_id').val(ids);
            $(".hidden_title").html(' "' + user + '" ');

            /*var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');*/

        });


        $('#deleteClient').find('#confirm_yes').on('click', function () {

            //set csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(" .modal-body #hidden_id").val();

            $.ajax({
                type: "POST",
                url: "{{ route('task.client-delete') }}",
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                    window.location.reload();
                }
            });
        });


    </script>
@stop
