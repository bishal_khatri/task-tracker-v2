@extends('layouts.app')
@section('page-title')
	Create Task
@stop

@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
	<style>
		.modal-dialog {
			transform: translate(0, -50%);
			top: 20%;
			margin: 0 auto;
		}
	</style>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('task.store') }}">
				<div class="white-box">
					<div class="row">
						<div class="col-md-8">
							{{ csrf_field() }}
							<div class="form-body">

								<div class="form-group{{ $errors->has('select_client') ? ' has-error' : '' }}">
									<label class="col-md-3 control-label">Select Client <span style="color:red;">*</span></label>
									<div class="col-md-7">
										<select name="select_client" class="js-example-basic-single js-states form-control" id="id_label_single">

											<option selected value=""  >Select Any Task Type</option>
											@foreach($client as $value)
												<option value="{{ $value->id }}"  @if(old('select_client') == $value->name) selected @endif >{{  $value->name  }}</option>
											@endforeach
										</select>

										@if ($errors->has('select_client'))
											<span class="help-block">
												<strong>{{ $errors->first('select_client') }}</strong>
											</span>
										@endif
									</div>

									<div class="col-md-2">
										<span>
											<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addClient" >
												<i class="fa fa-plus-square"></i>
											</button>
										</span>
									</div>

								</div>

								<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
									<label class="col-md-3 control-label">Title <span style="color:red;">*</span></label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="Title" name="title" value="{{ old('title') }}">
										@if ($errors->has('title'))
											<span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
										@endif
									</div>
								</div>
								
								<div class="form-group{{ $errors->has('deadline') ? ' has-error' : '' }}">
									<label class="col-md-3 control-label">Deadline <span style="color:red;">*</span></label>
									<div class="col-md-9">
										{{--<input type="date" id="deadline" class="form-control" placeholder="Standard" name="deadline" value="{{ old('deadline')}}">--}}
										<input type="number"  id="deadline" class="form-control" placeholder="Enter Deadline Day" name="deadline" value="{{ old('deadline')}}">
										@if ($errors->has('deadline'))
											<span class="help-block">
												<strong>{{ $errors->first('deadline') }}</strong>
											</span>
										@endif
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Task Details</label>
									<div class="col-md-9">
										<textarea class="form-control" name="description" rows="15" >{{old('description')}}</textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Task Priority <span style="color:red;">*</span></label>
									<div class="col-md-9">
										<select name="priority" class="form-control" >
											<option value="1"  {{ (old('priority') == 1) ? "selected" : '' }} >Normal</option>
											<option value="2"{{ (old('priority') == 2) ? "selected" : '' }}>Important </option>
											<option value="3" {{ (old('priority') == 3) ? "selected" : '' }}>Urgent</option>
										</select>
										@if ($errors->has('priority'))
											<span class="help-block">
												<strong>{{ $errors->first('priority') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Task Type</label>
									<div class="col-md-9">
										<select name="task_type" class="form-control" >
											<option selected value=""  >Select Any Task Type</option>
											<option value="new" {{ (old('task_type') == 'new') ? "selected" : '' }}>New</option>
											<option value="maintenance" {{ (old('task_type') == 'maintenance') ? "selected" : '' }}>Maintenance</option>
										</select>
										@if ($errors->has('task_type'))
											<span class="help-block">
												<strong>{{ $errors->first('task_type') }}</strong>
											</span>
										@endif
									</div>
								</div>


							</div>
						</div>
						<div class="col-md-4">
							<div class="portlet box green-meadow">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-gift"></i>Employee <span style="color:red;">*</span>
									</div>
								</div>
								<div class="portlet-body" style="overflow-y: scroll; height: 67vh;">
									<ul class="list-group">
										<li class="list-group-item">
											@foreach ($staff as $name)
												<div class="input-group form-control">
													<input type="checkbox" aria-label="..." name="staff[]" value="{{ $name->id }}" multiple="multiple" class="form-control-sm">
													<strong>
														{{ $name->full_name }} @if($name->whereTask->count() > 0)<span style="color:red;">({{$name->whereTask->count()}})</span>@endif
													</strong>
												</div>
												<br>
											@endforeach
										</li>
									</ul>
									@if ($errors->has('staff'))
										<span class="help-block" style="color:darkred;">
                                        	* {{ $errors->first('staff') }}
                                    	</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="white-box">
					<div class="row">
						<div class="col-md-12">
							<div class="form-actions">
								<button type="reset" class="btn default">Clear</button>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	@include('pages.task.modal')

@endsection

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

	<script>
		$('.js-example-basic-single').select2({
			placeholder: 'Select an option'
		});


		{{--@if($errors->count()> 0 )
				alert({{$errors->first('client_name') }});
			$("#addClient").modal("show");
		@endif--}}


	</script>
@stop