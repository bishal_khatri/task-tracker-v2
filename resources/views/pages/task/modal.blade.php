
<div id="showMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="margin-top: 200px;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> User Deleted
                </h4>
            </div>

            <div class="modal-body">
                <div class="error" style="display: none;">
                    <i class="fa fa-close"></i> &nbsp; Error While Deleting &nbsp;Task.
                </div>

                <div class="success" style="display: none;">
                    <i class="fa fa-check"></i> &nbsp; Task Deleted Successfully .
                </div>

            </div>
            <input type="hidden" id="hidden_id">
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-danger green confirm_yes" id="show_message"><i class="icon-check"></i> Ok
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

{{--delete task--}}
<div id="deleteTask" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content"  style="margin-top: 200px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
                </h4>
            </div>
            <div class="modal-body"> Do you want to delete <span class='hidden_title'>" "</span>?</div>
            <input type="text" id="hidden_id">
            <div class="modal-footer">
                <button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                    No
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

{{--client add form--}}

<div class="modal fade" id="addClient" tabindex="-1" role="dialog" aria-labelledby="client" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Add Client</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{route('task.client-store')}}" method="post">
                @csrf
                <div class="modal-body">

                    <div class="form-group {{ $errors->has('client_name') ? ' has-error' : '' }}">
                        <label class="control-label">Client Name <span style="color:red;">*</span></label>
                        <input type="text"  class="form-control" placeholder="Enter Client Name" name="client_name" value="{{ old('client_name')}}">

                        @if ($errors->has('client_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('client_name') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                        <label class="control-label">Client Number <span style="color:red;">*</span></label>
                            <input type="number"  class="form-control" placeholder="Enter Client Contact Number" name="number" value="{{ old('number')}}">
                            @if ($errors->has('number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                            @endif
                    </div>

                    <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                        <label class="control-label">Latitude</label>
                            <input type="text"  class="form-control" placeholder="Enter Client Latitude" name="latitude" value="{{ old('latitude')}}">

                            @if ($errors->has('latitude'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('latitude') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                        <label class=" control-label">Longitude</label>
                            <input type="text"  class="form-control" placeholder="Enter Client Longitude" name="longitude" value="{{ old('longitude')}}">
                            @if ($errors->has('longitude'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('longitude') }}</strong>
                                    </span>
                            @endif
                    </div>
            </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-check"></i> Save Client</button>
                </div>

            </form>
        </div>
    </div>
</div>

{{--client delete --}}
<div id="deleteClient" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content"  style="margin-top: 200px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently </h4>
            </div>
            <div class="modal-body">
                <p>Do you want to delete <span class='hidden_title'>" "</span>?</p>
                <input type="hidden" id="hidden_id">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                    No
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>