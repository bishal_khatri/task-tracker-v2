@extends('layouts.app')
@section('page-title')
   Edit Task
@stop
@section('content')

    <div class="white-box">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('task.edit',$task->id) }}">
                    <div class="row">
                        <div class="col-md-8">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Title <span style="color:red;">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="Title" name="title" value="{{old('title') ?? $task->name}}">
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                                        @endif
                                    </div>
                                </div>
                    
                                <div class="form-group{{ $errors->has('deadline') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Deadline <span style="color:red;">*</span></label>
                                    <div class="col-md-9">
                                        <input type="date" id="deadline" class="form-control" placeholder="Standard" name="deadline" value="{{old('deadline') ?? $task->deadline }}">
                                        @if ($errors->has('deadline'))
                                            <span class="help-block">
                                <strong>{{ $errors->first('deadline') }}</strong>
                            </span>
                                        @endif
                                    </div>
                                </div>
                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Task Details</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="description" rows="15"> {{old('description') ?? $task->description }} </textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Task Priority <span style="color:red;">*</span></label>
                                    <div class="col-md-9">
                                        <select name="priority" class="form-control" >
                                            <option value="1"
                                                    @if(empty(old('priority')))
                                                        {{ ($task->priority == 1) ? "selected" : '' }}
                                                    @else
                                                        {{ (old('priority')  == 1) ? "selected" : '' }}
                                                    @endif
                                                   >Normal</option>
                                            <option value="2"
                                                    @if(empty(old('priority')))
                                                        {{ ($task->priority == 2) ? "selected" : '' }}
                                                    @else
                                                        {{ (old('priority')  == 2) ? "selected" : '' }}
                                                    @endif
                                                  >Important </option>
                                            <option value="3"
                                                    @if(empty(old('priority')))
                                                        {{ ($task->priority == 3) ? "selected" : '' }}
                                                    @else
                                                        {{ (old('priority')  == 3) ? "selected" : '' }}
                                                    @endif
                                                    >Urgent</option>
                                        </select>
                                        @if ($errors->has('priority'))
                                            <span class="help-block">
												<strong>{{ $errors->first('priority') }}</strong>
											</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Task Type</label>
                                    <div class="col-md-9">
                                        <select name="task_type" class="form-control" >
                                            <option selected value="" {{(old('task_type') == "") ? "selected" : ''}} >{{old('task_type')}}Select Any Task Type</option>
                                            <option value="new"
                                                @if(empty(old('task_type')))
                                                    {{ ($task->task_type == "new") ? "selected" : '' }}
                                                @else
                                                    {{ (old('task_type')  == "new") ? "selected" : '' }}
                                                @endif
                                                >New</option>
                                            <option value="maintenance"
                                                @if(empty(old('task_type')))
                                                    {{ ($task->task_type == "maintenance") ? "selected" : '' }}
                                                @else
                                                    {{ (old('task_type')  == "maintenance") ? "selected" : '' }}
                                                @endif
                                                >Maintenance</option>
                                        </select>
                                        @if ($errors->has('task_type'))
                                            <span class="help-block">
												<strong>{{ $errors->first('task_type') }}</strong>
											</span>
                                        @endif
                                    </div>
                                </div>
                    
                                <div class="form-group{{ $errors->has('select_client') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Select Client <span style="color:red;">*</span></label>
                                    <div class="col-md-7">
                                        <select name="select_client" class="js-example-basic-single js-states form-control" id="id_label_single">

                                            <option selected value=""  >Select Any Task Type</option>
                                            @foreach($all_client as $value)
                                                <option value="{{ $value->id }}"  @if($task->client_id== $value->id) selected  @endif >{{  $value->name  }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('select_client'))
                                            <span class="help-block">
												<strong>{{ $errors->first('select_client') }}</strong>,y
                                        @endif
                                    </div>

                                    <div class="col-md-2">
										<span>
											<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addClient" >
												<i class="fa fa-plus-square"></i>
											</button>
										</span>
                                    </div>

                                </div>
                    

                            </div>
                        </div>

                        <div class="col-md-4">
                            <h2 class="control-label" style="text-align:left;"> Assigned Employees</h2>
                            <br>
                            <ul>
                                    @foreach($task->users as $value)
                                    <li>
                                        <hr>
                                        {{ $value->full_name }}
                                        <hr>

                                    </li>
                                    @endforeach
                            </ul>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2"></div>

                            <div class="col-md-9">
                                <div class="form-group ">
                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Update</button>
                                    <a href="{{ URL::previous() }}" type="button" class="btn default"> <i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>

                        </div>

                    </div>
        

                </form>
            </div>
        </div>
    </div>

@endsection