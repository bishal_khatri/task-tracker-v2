@extends('layouts.app')
@section('page-title')
    Task Listing
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/jquery.dataTables.min.css') }}">
    <link href="{{ asset(STATIC_DIR.'css/style.css')}}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
    <div class="white-box">
        <div class="row" style="min-height: 83vh !important;">
            <div class="col-lg-12">
                <div class="main-box clearfix">
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Assigned to</th>
                                <th>Assigned By</th>
                                <th>Created At</th>
                                <th>Status</th>
                                <th>Deadline</th>
                                <th>Task Priority</th>
                                <th class="col-sm-2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($task))
                                @foreach($task->sortBy('status',0) as $val)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            @if(strlen($val->name) >30)
                                            {{substr($val->name,0,50)}}
                                            </br>
                                            {{substr($val->name,50,strlen($val->name))}}

                                            @else
                                                {{$val->name}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($val->users->count() == 0)
                                                <strong>No Staffs Assigned</strong>
                                            @else
                                                @foreach($val->users as $value)
                                                    <p>{{$value->full_name}}</p>
                                                @endforeach
                                            @endif
                                        </td>

                                        <td>
                                            <p>{{$val->author->full_name}}</p>
                                            {{--@foreach($val->author as $value)--}}
                                            {{--<p>{{$value->full_name}}</p>--}}
                                            {{--@endforeach--}}
                                        </td>

                                        <td>{{ $val->created_at->diffForHumans() }}</td>

                                        <td class="center">
                                            @if($val->status==0)
                                                <p class="list-group-item-text">
                                                    <span class="label label-primary">New</span>
                                                </p>
                                            @elseif($val->status==1)
                                                <p class="list-group-item-text">
                                                    <span class="label label-warning">Opened</span>
                                                </p>
                                            @elseif($val->status==2)
                                                <p class="list-group-item-text">
                                                    <span class="label label-danger">Pending</span>
                                                </p>
                                            @elseif($val->status==3)
                                                <p class="list-group-item-text">
                                                    <span class="label label-success">Completed</span>
                                                </p>
                                            @endif
                                        </td>
                                        <td>
                                            {{$val->deadline}}
                                        </td>

                                        <td>
                                            @if($val->priority == 1)
                                                <span class="label label-default">Normal</span>
                                            @elseif($val->priority == 2)
                                                <span class="label label-warning">Important</span>
                                            @else
                                                <span class="label label-danger">Urgent</span>
                                            @endif
                                        </td>

                                        <td style="width: 12%;" >

                                            <a href="{{ route('task.view',$val->id) }}" class="table-link" title="View details">
                                                <span class="fa-stack">
                                                    <i class="fa fa-square fa-stack-2x" ></i>
                                                    <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </a>

                                            <a href="{{ route('task.edit',$val->id) }}" class="table-link" title="Edit">
                                                <span class="fa-stack">
                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                    <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </a>

                                            <a href="{{ route('task.delete',$val->id) }}" data-key="" class="table-link danger delete" title="Delete">
                                                <span class="fa-stack">
                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'jquery/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#myTable').dataTable( {
                "pageLength": 50,
                "lengthMenu": [ 10, 25, 50, 75, 100 ]
            } );
        } );
    </script>
@endsection