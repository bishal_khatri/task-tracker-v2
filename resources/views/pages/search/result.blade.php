@extends('layouts.app')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Search result for <small>"{{ Request::input('search') }}"</small></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                	<div class="col-lg-12">
                	@if ($results_task->count() OR  $results_user->count())
                		
                        @foreach($results_task as $data)
                         <div class="panel panel-default">
	                        <div class="panel-heading">
	                            <h4 class="panel-title">
	                                <a  href="{{ route('task.view',$data->id) }}">{{ $data->name }}</a> <a href="{{ route('task.view',$data->id) }}" class="btn btn-default">view</a>
	                            </h4>

	                        </div>
	                       
	                    </div>
	                    @endforeach
                  		
                        @foreach($results_user as $data)
                         <div class="panel panel-default">
	                        <div class="panel-heading">
	                            <h4 class="panel-title">
	                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">{{ $data->full_name }}</a> <a href="{{ route('profile.view',$data->id) }}" class="btn btn-default">view</a>
	                            </h4>

	                        </div>
	                        <div id="collapseTwo" class="panel-collapse collapse">
	                            <div class="panel-body">
	                                {{ $data->user_name }}
	                            </div>
	                        </div>
	                    </div>
	                    @endforeach

                		@else
	                   No result found!! 
	                    
	                    @endif

                    </div>
                </div>
            </div>

 
@endsection