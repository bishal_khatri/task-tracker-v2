
<!-- Modal -->
{{--@if (Request::is("/") && \Auth::user()->user_type == 0)--}}
@if (Request::is("/") && isset($users))
<div class="modal fade" id="homemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalCenterTitle">Assigned Task</h2>

            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered user-list">
                                <thead>
                                        <tr>
                                            <td> S.N.</td>
                                            <td> Assigned Task:</td>
                                            <td> Project Deadline:</td>
                                            <td> Status</td>
                                        </tr>
                                </thead>

                                <tbody>
                                    @foreach($user->where('status','=','0') as $task)
                                        <tr>
                                            <td>
                                                {{$loop->iteration}}
                                            </td>
                                            <td>
                                                {{$task->name}}
                                            </td>
                                            <th>
                                                <span class="label label-sm label-danger">
                                                    {{$task->deadline}}
                                                </span>
                                            </th>
                                            <th>
                                                @if($task->status == 1)
                                                        Opened
                                                @elseif($task->status == 2)
                                                    Pending
                                                @else
                                                    New Task
                                                @endif
                                            </th>
                                        </tr>
                                    @endforeach
                                    @if($user->where('status','=','0')->count() == 0)
                                        <tr  >
                                            <th colspan="4" style="text-align: center">No New Task Assigned.</th>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok ! Got It.</button>
            </div>
        </div>
    </div>
</div>
@endif


{{--remarks modal--}}
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('task.store_remarks') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="exampleModalLabel">Remarks</h3>

                </div>
                <div class="modal-body">
                        <input type="hidden" class="form-control" name="task_status" id="task_status">
                        <input type="hidden" class="form-control" name="task_id" id="task_id">

                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Message (optional) :</label>
                            <textarea class="form-control" id="message-text" rows="7" name="remarks"></textarea>
                        </div>


                        <div class="form-group" id="file_upload" style="display: none;">
                            <label for="message-text" class="col-form-label">File: (optional) :</label>
                            <input type="file" class="form-control" name="file_upload">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Change Status">
                </div>
            </form>

        </div>
    </div>
</div>