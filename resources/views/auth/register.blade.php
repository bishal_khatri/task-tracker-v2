@extends('layouts.app')
@section('page-title')
    Register New Staff
@stop
@section('content')
    <div class="white-box">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Full Name" name="full_name" value="{{old('full_name')}}">
                                @if ($errors->has('full_name'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('full_name') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">User Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="User Name" name="user_name" value="{{old('user_name')}}">
                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('user_name') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Confirm Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" placeholder="Re-type password" name="password_confirmation">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Select User Type</label>
                            <div class="col-md-9">
                                <select class="form-control" name="user_type" required>
                                    <option value="" disabled="" selected>--Select Any One --</option>
                                    <option value="1">Admin</option>
                                    <option value="0">Staff</option>
                                    <option value="2">Account</option>
                                </select>
                            </div>
                        </div>
                    
                    
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn default">Clear</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
