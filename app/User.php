<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'contact','join_date','department','image','user_type','user_name','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }


//    public function task()
//    {
//        return $this->belongsToMany(Task::class)->where('status','=','3');
//    }

    //retrieve all task excluding status 3 (completed).
    public function whereTask()
    {
        return $this->belongsToMany(Task::class)->where('status','!=','3');
    }

    //retrieve all completed task only.
    public function completedTask()
    {
        return $this->belongsToMany(Task::class)->where('status','=','3');
    }

    //retrieve all new task only.
    public function newTask()
    {
        return $this->belongsToMany(Task::class)->where('status','=','0');
    }

   public function isAdmin()
   {
      return $this->user_type;
   }

//   public function searchableAs()
//    {
//        return 'user';
//    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function getAvatarUrl()
    {
        return "https://www.gravatar.com/avatar/{{ md5($this->user_name) }}?d=mm&s=40";
    }
}
