<?php

namespace App\Http\Controllers\Auth;

use App\Helper\Tools;
use File;
use Auth;
use Hash;
use Illuminate\Support\Facades\Storage;
use Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/staff';

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);
        return redirect()->route('staff.show');

//        return $this->registered($request, $user)
//                        ?: redirect($this->redirectPath());
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin'],['except'=>['getEdit','getView','update']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'full_name' => 'required|string|max:255',
            'user_name' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'user_type' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'full_name' => $data['full_name'],
            'user_type' => 0,
            'user_name' => $data['user_name'],
            'password' => bcrypt($data['password']),
            'user_type' => $data['user_type']

        ]);
    }

    public function show()
    {
        $staff = User::paginate(10);
        return view('pages.staff.showstaff')->with('staff', $staff);
    }

    public function getEdit($id)
    {
        $staff = User::find($id);
        return view('pages.staff.edit')->with('staff', $staff);
    }

    public function update(Request $request, $id)
    {

        // validation of name field
        $this->validate($request, [
            'full_name' => 'required',
            'user_name' => 'required',
            'profile_image' => 'mimes:jpeg,jpg,png|max:2048',
            ]);

        // class initilization
        $staff = User::find($id);
        // requesting name and image from form
        // $staff->profile_image=$request->input('profile_image');
        // $staff->citizenship_image=$request->input('citizenship_image');
        $staff->full_name=$request->input('full_name');
        $staff->user_name=$request->input('user_name');
        $staff->contact=$request->input('contact');
        $staff->permanent_address=$request->input('permanent_address');
        $staff->temporary_address=$request->input('temporary_address');
        $staff->email=$request->input('email');
        $staff->facebook_link=$request->input('facebook_link');
        $staff->linkedin_link=$request->input('linkedin_link');
        $staff->join_date=$request->input('join_date');
        $staff->department=$request->input('department');


        // receive image
        if(isset($request->profile_image)){

            //first delete previous profile picture from storage
            Storage::delete('public/'.$staff->profile_image);
            $path = $request->file('profile_image')->store('files/'.$staff->id.'/avatar','public');
            $staff->profile_image = $path;

        }

        if(isset($request->citizenship_image)){

            //first delete previous profile picture from storage
            Storage::delete('public/'.$staff->citizenship_image);
            $path = $request->file('citizenship_image')->store('files/'.$staff->id.'/citizenship','public');
            $staff->citizenship_image = $path;

        }


        $staff->save();

        Session::flash('success', 'Staff details was successfully updated');

        return redirect()->route('profile.view',$id);
    }

    public function getView($id)
    {
        $staff = User::find($id);
//        dd($staff);
        $assigned = User::find($id)->tasks()->paginate(5);
        // $assigned =$assign->sortByDesc('id');
        return view('pages.staff.view')->with('staff', $staff)->with('assigned', $assigned);
    }


    public function delete(Request $request)
    {



        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;

            return response()->json($responseData,$status);
        }

        //deleting staff
        $data = User::find($request->id);
        Storage::delete('public/'.$data->profile_image);
        Storage::delete('public/'.$data->citizenship_image);

        $data->delete();

//        \Session::flash('success','User Deleted Successfully');
        $responseData = Tools::setResponse('success', 'Staff Deleted Successfully', '', '');
        return response($responseData,200);

    }

    public function editRole()
    {
        $staff = User::all();
        return view('pages.staff.role')->with('staff', $staff);
    }

    public function updateRole(Request $request,$id)
    {
        $this->validate($request, [
            'user_type' => 'required',
            ]);

        $role = User::find($id);
        $role->user_type=$request->user_type;
        $role->save();

        Session::flash('success', 'Role successfully CHANGED');
        return redirect()->back();

    }

    

}
