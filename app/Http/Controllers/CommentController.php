<?php

namespace App\Http\Controllers;

use Auth;
use App\Task;
use App\User;
use Session;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $taskId)
    {
    	$task = Task::find($taskId);
    	$user = Auth::user()->id;
    	$this->validate($request, [
    		'comment' => 'required',
    		]);
    	$comment = new Comment;
    	$comment->comment=$request->comment;
    	$comment->task()->associate($task);
    	$comment->user()->associate($user);
    	$comment->save();

    	Session::flash('success', 'Comment posted successfully');

    	return redirect()->back();
    }


    public function delete($id)
    {
        $delete = Comment::find($id);
        $delete->delete();

        Session::flash('success', 'Comment DELETED successfully');
        return redirect()->back();
    }


    public function edit($id)
    {
        $comment = Comment::find($id);
      
        return view('pages.task.editComment')->with('comment', $comment);
    }

    public function update(Request $request,$id)
    {
        $comment = Comment::find($id);
        $comment->comment=$request->comment_edit;
        $comment->save();

 

        return redirect()->route('task.view',$comment->task->id);
    }
}
