<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use App\Task;
use App\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    
    public function view($id)
    {
    	//$task = Task::orderBy('id', 'desc')->paginate(10);
    	//$task_completed = Task::where('status', '=', 3)->orderBy('id', 'desc')->paginate(10);

    	$profile = User::find($id);
        $assigned = User::find($id)->tasks()->paginate(5);
        $assigned_open = User::find($id)->tasks()->where('status', '=', 1)->paginate(5);
        $assigned_pending = User::find($id)->tasks()->where('status', '=', 2)->paginate(5);
        $assigned_completed = User::find($id)->tasks()->where('status', '=', 3)->paginate(5);
    	return view('pages.staff.profile')
    			->with('profile',$profile)
    			->with('assigned',$assigned)
    			->with('assigned_open',$assigned_open)
    			->with('assigned_pending',$assigned_pending)
    			->with('assigned_completed',$assigned_completed);
    }

    public function editPassword()
    {
        return view('pages.staff.password');
    }

    public function updatePassword(Request $request,$id)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
            'password_c' => 'required|min:6',
            ]);
        $change = User::find($id);
        if (Hash::check(Input::get('old_password'), $change->password))
        { 
            if ($request->password==$request->password_c) {
                
                $change->password=bcrypt($request->password);
                $change->save();

                Session::flash('success', 'Password successfully CHANGED');
                Auth::logout();
                return redirect()->route('home');
            }
            else
            {
                Session::flash('error', 'Password did not match');
                return redirect()->back();
            }
        }
        else
        {
            Session::flash('error', 'Old password did not match');
            return redirect()->back()->withErrors(['old_password' => 'Your password is incorrect.']);
        }
    }
}
