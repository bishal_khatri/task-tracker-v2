<?php

namespace App\Http\Controllers;

use App\TaskUser;
use DB;
use Auth;
use App\User;
use App\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        dd(STATIC_DIR);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //user
       /* if(Auth::user()->user_type == 0){
            $users = User::where('id',Auth::user()->id)->with('tasks','tasks.author')->first();

//            $formData['users'] = User::where('id',Auth::user()->id)->with('tasks','tasks.author')->first();
//            dd($formData);
            if (isset($request->type)){
                $formData['tasks'] = TaskUser::select('t.*','u.full_name')
                    ->join('tasks as t', 't.id','task_user.task_id')
                    ->join('users as u','task_user.user_id','u.id')
                    ->where('u.id',\Auth::user()->id)
                    ->where('status',$request->type)
                    ->get();
            }
            else{
                $formData['tasks'] = TaskUser::select('t.*','u.full_name')
                    ->join('tasks as t', 't.id','task_user.task_id')
                    ->join('users as u','task_user.user_id','u.id')
                    ->where('u.id',\Auth::user()->id)
                    ->get();
            }

            $formData['new'] = (count($users->tasks->where('status',0)));
            $formData['opened'] = (count($users->tasks->where('status',1)));
            $formData['pending'] = (count($users->tasks->where('status',2)));
            $formData['completed'] = (count($users->tasks->where('status',3)));

            return view('home',$formData);
        }*/
        //account user
//        else

            if(Auth::user()->user_type == 2){
                $formData['tasks'] = TaskUser::select('t.*','u.full_name','c.name as client_name','c.number')
                    ->join('tasks as t', 't.id','task_user.task_id')
                    ->join('users as u','task_user.user_id','u.id')
                    ->join('clients as c','t.client_id','c.id')
                    ->where('status',3)
//                    ->where('u.id',\Auth::user()->id)
                    ->get();
//                dd($formData);
            return view('home',$formData);
        }
        //admin user
        else{
//            dd($request->type);

            $users = User::where('id',Auth::user()->id)->with('whereTask','tasks.author')->first();
            if (isset($request->type)){
                $tasks = TaskUser::select('t.*','u.full_name','c.name as client_name','c.number')
                                ->join('tasks as t', 't.id','task_user.task_id')
                                ->join('users as u','task_user.user_id','u.id')
                                ->join('clients as c','t.client_id','c.id')

                                ->where('status',$request->type)
                                ->where('u.id',\Auth::user()->id)
                                ->get();

            }else{
                //retrieving task assigned to admin.
                $tasks = TaskUser::select('t.*','u.full_name','c.name as client_name', 'c.number')
                    ->join('tasks as t', 't.id','=','task_user.task_id')
                    ->join('users as u','u.id','t.created_by')

                    ->join('clients as c','t.client_id','c.id')

                    ->where('task_user.user_id',\Auth::user()->id)
//                    ->where('t.status','!=','3')
                    ->get();

//                dd($tasks);

            }

               /* $tasks = TaskUser::select('t.*','u.full_name','c.name as client_name','c.number')
                    ->join('tasks as t', 't.id','=','task_user.task_id')
                    ->join('users as u','t.created_by','=','u.id')
                    ->join('clients as c','t.client_id','c.id')

                    ->where('u.id',\Auth::user()->id)
                    ->where('t.status','!=','3')
                    ->get();

            }*/
            $formData['tasks'] = $tasks;
            $formData['new'] = (count($users->tasks->where('status',0)));
            $formData['opened'] = (count($users->tasks->where('status',1)));
            $formData['pending'] = (count($users->tasks->where('status',2)));
            $formData['completed'] = (count($users->tasks->where('status',3)));
//            dd($formData);
            return view('home',$formData);
        }
    }

}
