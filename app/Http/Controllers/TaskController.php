<?php

namespace App\Http\Controllers;

use App\Client;
use App\Helper\Tools;
use App\TaskNotification;
use App\TaskRemark;
use App\TaskUser;
use App\User;
use Illuminate\Support\Facades\Validator;

use Session;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function getAllClient(){
        $data['client'] = Client::all();
        return view('pages.task.clients',$data);
    }

    public function storeClient(Request $request){
        $this->validate($request, [
            'client_name' => 'required|max:100',
            'number' => 'required|numeric|min:6',

        ]);

        if(isset($request->latitude)){
            $this->validate($request,[
                'latitude' => 'numeric|between:0,999.99'
            ]);
        }

        if(isset($request->longitude)){
            $this->validate($request,[
                'longitude' => 'numeric|between:0,999.99'

            ]);
        }

        $client = new Client();
        $client->name = $request->client_name;
        $client->number = $request->number;
        $client->latitude = $request->latitude;
        $client->longitude = $request->longitude;
        $client->created_at = date('Y-m-d H:i:s');
        $client->save();

        \Session::flash('success','Client Added Successfully');
        return redirect()->back();
    }

    public function deleteClient(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;

            return response()->json($responseData,$status);
        }

        //deleting task
        $data = Client::find($request->id);
        $data->delete();

        $responseData = Tools::setResponse('success', 'User Deleted Successfully', '', '');
        return response($responseData,200);
    }

    public function getClientById(Request $request){
        $client = Client::find($request->client_id);
        return json_encode($client);
    }

    public function editClient(Request $request){
        $this->validate($request, [
            'client_id' => 'required',
            'client_name' => 'required|max:100',
            'number' => 'required|numeric|min:6',

        ]);

        if(isset($request->latitude)){
            $this->validate($request,[
                'latitude' => 'numeric|between:0,999.99'
            ]);
        }

        if(isset($request->longitude)){
            $this->validate($request,[
                'longitude' => 'numeric|between:0,999.99'

            ]);
        }

        $client =  Client::find($request->client_id);
        $client->name = $request->client_name;
        $client->number = $request->number;
        $client->latitude = $request->latitude;
        $client->longitude = $request->longitude;
        $client->save();

        \Session::flash('success','Client Updated Successfully');
        return redirect()->back();
    }

    public function create()
    {
        $data['staff'] =  User::with('whereTask')->get();
        $data['client'] = Client::orderby('id','desc')->get();

        return view('pages.task.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'deadline' => 'required|numeric',
            'priority' => 'required',
            'select_client' => 'required',

//            'client_name' => 'required',
            'staff' => 'required'
        ]);


        $task = new Task;
        $task->name = $request ->title;
        $task->deadline=date('Y-m-d', strtotime("+".$request->deadline." days") );
        $task->description=$request->description;

        $task->priority=$request->priority;
        $task->task_type=$request->task_type;

        $task->client_id = $request->select_client;

        $task->created_at = date('Y-m-d H:i:s');
        $task->created_by = \Auth::user()->id;
        $task->save();

        // attached staff info from user table in the task found above
        $task->users()->sync($request->staff, false);

        if(isset($request->staff)){
            //retrieve assigned user.
            $assigned_user = array();

            foreach ($request->staff as $value){
                $assigned_user[] = User::find($value);
            }

            foreach ($assigned_user as  $value) {
                $this->notification($value->firebase_token, $request->title,$request->description);

                $notification = new TaskNotification();
                $notification->name = $request->title;
                $notification->user_id = $value->id;
                $notification->description = $request->description;
                $notification->save();
            }

            Session::flash('success','New Task Successfully Created With Notification To App User');

        }



        Session::flash('success','New Task Successfully Created');
        return redirect()->route('task.show');
    }

    public function show()
    {
        $task = Task::orderBy('id', 'desc')->where('status','!=',3)->with('users','author','clients')->get();
//        dd($task);
        return view('pages.task.index')->with('task', $task);
    }

    public function showCompletedTask(){
        // $task = Task::orderBy('id', 'desc')->where('status',3)->with('users','author')->get();
        $task = Task::orderBy('updated_at', 'desc')->where('status',3)->with('users','author','clients')->get();
        // dd($task);

        return view('pages.task.index')->with('task', $task);

    }

    //api call from assign
    public function getAssignedUserById(Request $request){

        if(isset($_GET['id'])){
            $task = Task::where('id',$_GET['id'])->with('users')->first();
            $response_data = Tools::setResponse('success','Fetching users data successfull',$task,'');
        }
        else{
            $response_data = Tools::setResponse('error','Error while Fetching users data successfull','','');
        }
        return ($response_data);

    }

    public function getAssign()
    {
        $staff = User::all();
        $task = Task::with('users')->get();
//    	dd($task);
        return view('pages.task.assign')->with('task', $task)->with('staff', $staff);
    }

    public function assign(Request $request)
    {
        $this->validate($request,[
            'task' => 'required',
        ]);

        //deleting previous assigned user.
        $task_user = \DB::table('task_user')->where('task_id',$request->task)->delete();


        // grabbed $request->task from task selection and used it to find task in task table
        $task = Task::find($request->task);

        // attached staff info from user table in the task found above
        $task->users()->sync($request->staff, false);

        // set session
        Session::flash('success', 'Task has been assigned successfully');
        return redirect()->route('task.assign');
    }

    public function viewTask($id)
    {
        $tasks = Task::find($id);
        $assigned = Task::find($id)->users;
        $remarks = TaskRemark::where('task_id',$id)->get();
//        dd($remarks);
        return view('pages.task.view')->with('tasks', $tasks)->with('assigned', $assigned)->with('remarks',$remarks);
    }

    public function status(Request $request,$id)
    {
        // $getStatus = Task::find($id);
        $status = Task::find($id);
        $status->status=$request->input('status');
        // $staff->department=$request->input('department');

        $status->save();
        Session::flash('success', 'Task status has been changed');
        return redirect()->back();
    }

    public function edit($id)
    {
        $data['task'] = Task::where('id',$id)->with('users')->first();
        $data['all_client'] = Client::orderby('id','desc')->get();
//        $data['client'] = Client::orderby('id','desc')->where('id',$id)->first();
//        dd($data);

        return view('pages.task.edit',$data);
    }

    public function update(Request $request,$id)
    {
        dd($request->all());
        $this->validate($request, [
            'title' => 'required|max:100',
            'deadline' => 'required|date',
            'priority' => 'required',
            'client_name' => 'required'
        ]);

        //validating optional field
        if(isset($request->client_number)){
            $this->validate($request,[
                'client_number' => 'min:9'
            ]);
        }

        if(isset($request->client_latitude)){
            $this->validate($request,[
                'client_latitude' => 'numeric|between:0,999.99'
            ]);
        }

        if(isset($request->client_longitude)){
            $this->validate($request,[
                'client_longitude' => 'numeric|between:0,999.99'

            ]);
        }
        $task = Task::find($id);

        $task->name=$request->input('title');
        $task->deadline=$request->input('deadline');
        $task->description=$request->input('description');

        $task->priority = $request->priority;
        $task->task_type = $request->task_type;


        $task->client_name=$request->client_name;
        $task->client_number=$request->client_number;
        $task->client_latitude=$request->client_latitude;
        $task->client_longitude=$request->client_longitude;

        $task->save();

        Session::flash('success', 'Task has been Updated');
        return redirect()->route('task.show');
    }

    public function delete(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;

            return response()->json($responseData,$status);
        }

        //deleting task
        $data = Task::find($request->id);
        $data->delete();

        $responseData = Tools::setResponse('success', 'User Deleted Successfully', '', '');
        return response($responseData,200);

    }

    public function addRemarks(Request $request){
        $this->validate($request,[
//            'remarks' => 'max:100',
//            'file_upload' => 'max:5120'
        ]);
        $remarks = new TaskRemark();
        $remarks->task_id = $request->task_id;
        $remarks->remarks = $request->remarks;
        $remarks->status = $request->task_status;
        $remarks->save();

        $task = Task::find($request->task_id);

        //upload in server folder.
        if(isset($request->file_upload)){
            $upload = $request->file('file_upload')->store('files/'.$request->task_id.'/completed_task', 'public');
            $task->file = $upload;
        }
        $task->status = $request->task_status;
        $task->save();

        return  redirect()->route('home');
    }

    //ajax call to fetch task based on user id.
    public function getTaskByUserId(){
        $user = TaskUser::join('tasks as t','t.id','task_user.task_id')->select('t.*')
            ->where('task_user.user_id',$_GET['user_id'])
            ->where('t.status','!=',3)
            ->get();
        return response()->json($user);
    }


    //notification to firebase using crud operation.
    public function notification($token, $title,$description = null)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;

        $notification = [
            'title' => ucfirst($title),
//            'click_action' => 'notification',
            'text' => $description,
//            'assigned_by' => 'yonzon',
            'sound' => 'notification.mp3',
        ];

        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key= AAAA8iYzy1M:APA91bFgJiofpoZ_zn9xCGNHP8d16EAQw-hMKCB0X1pkJbi1DYX7Jr8_6WrETFXob8BWfuCUluUBqQfiRySDNj3egAxP4PtsHnOS1fp4HPnSOAjGV2kz377BiY32VJLYxkvw1c7rVDtS',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        curl_close($ch);

        return true;
    }

}

