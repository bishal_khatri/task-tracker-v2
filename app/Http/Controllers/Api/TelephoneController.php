<?php

namespace App\Http\Controllers\Api;

use App\Helper\Tools;
use App\Telephone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TelephoneController extends Controller
{
    public function showAllTelephoneDirectory(){
        $telephone = Telephone::all();

        $responseData = Tools::setResponse('success','Telephone Listing Successfull', $telephone,'');
        $status = 200;
        return response()->json($responseData,$status);


    }
}
