<?php

namespace App\Http\Controllers\Api;

use App\Helper\Tools;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail','Missing Parameter', '','');
            $status = 422;
            return response()->json($responseData,$status);

        }
            //check user exists on database or not.
            $user = User::where('user_name',$request->username)->first();


            //if user is null.
            if(is_null($user)){
                $responseData = Tools::setResponse('fail','Login Credentials not matched.', '','');
                $status = 404;
                return response()->json($responseData,$status);
            }
            //check if password match or not
            if (\Hash::check($request->password, $user->password)) {
                //send user data with assigned task.

                $user = User::where('user_name',$request->username)->first();
                $user->firebase_token = $request->firebase_token;
                $user->save();
                $responseData = Tools::setResponse('success','Login Successfull.', $user,'');
                $status = 200;

                return response()->json($responseData,$status);
            }
            else{
                $responseData = Tools::setResponse('fail','Login Credentials not matched.', '','');
                $status = 404;
                return response()->json($responseData,$status);

            }
        }

}
