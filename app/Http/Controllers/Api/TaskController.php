<?php

namespace App\Http\Controllers\Api;

use App\Client;
use App\Helper\Tools;
use App\Task;
use App\TaskNotification;
use App\TaskRemark;
use App\TaskUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function clientList(){
        $data['client'] = Client::orderby('id','desc')->get();

        $responseData = Tools::setResponse('success','Client Listed Successfully', $data,'');
        return response()->json($responseData,200);
    }


    public function storeTask(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
            'priority' => 'required',
            'client_id'=> 'required',
            'deadline' => 'required|numeric',
            'staff' => 'required',
            'created_by' => 'required'
        ]);


        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;
            return response()->json($responseData, $status);
        }

        //adding new task

        $task = new Task;
        $task->name = $request->name;
        $task->deadline = date('Y-m-d', strtotime("+" . $request->deadline . " days"));
        $task->description = $request->description;

        $task->priority = $request->priority;
        $task->task_type = $request->task_type;

        $task->client_id= $request->client_id;


        $task->created_at = date('Y-m-d H:i:s');
        $task->created_by = $request->created_by;
        $task->save();

        //adding assigned staff to relationship
        //retrieve json encode employee id

        $decodedArray = json_decode($request->staff,true);
        $staff_id = array();

        foreach ($decodedArray['staff'] as $value) {
            $staff_id[] = $value['staff_id'];
        }

        // attached staff info from user table in the task found above
        $task->users()->sync($staff_id, false);


        //sending notification to the assigned staff about the new task

        if (isset($request->staff)) {
            //retrieve assigned user.
            $assigned_user = array();

            foreach ($staff_id as $value) {
                $assigned_user[] = User::find($value);
            }

            foreach ($assigned_user as $value) {
                $this->notification($value->firebase_token, $request->title, $request->description);

                $notification = new TaskNotification();
                $notification->name = $request->name;
                $notification->user_id = $value->id;
                $notification->description = $request->description;
                $notification->save();
            }


        }

        $responseData = Tools::setResponse('success','Task Added Successfully', '','');
        return response()->json($responseData,200);
    }

    public function changeStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'task_id' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail','Missing Parameter', '','');
            $status = 422;
            return response()->json($responseData,$status);

        }


//        if(isset($request->remarks)){
        $remarks = new TaskRemark();
        $remarks->task_id = $request->task_id;
        $remarks->remarks = $request->remarks;
        $remarks->status = $request->status;
        $remarks->save();
//        }

        //upload file in server folder.
        $task = Task::find($request->task_id);

        if(isset($request->file_upload)){
            $upload = $request->file('file_upload')->store('files/'.$request->task_id.'/completed_task', 'public');
            $task->file = $upload;

        }

        $task->status = $request->status;
        $task->save();


        if(isset($request->file_upload)){
            $responseData = Tools::setResponse('success','Status Changed Successfully with remarks and File Upload', '','');
            $status = 200;
        }
        else if(isset($request->remarks)){
            $responseData = Tools::setResponse('success','Status Changed Successfully with remarks.', '','');
            $status = 200;
        }
        else{
            $responseData = Tools::setResponse('success','Status Changed Successfully', '','');
            $status = 200;
        }

        return response()->json($responseData,$status);
    }

    public function getTaskByUserId(){

        if(!isset($_GET['user_id'])){
            $responseData = Tools::setResponse('fail','Missing Parameter', '','');
            $status = 422;
            return response()->json($responseData,$status);
        }

        $user_id =$_GET['user_id'];
        //fetching all task except completed one.
        $user = User::where('id',$user_id)->with('whereTask.author')->with('whereTask.clients')->first();
        $user->makeHidden(['contact','email','permanent_address','temporary_address','facebook_link','google_link','twitter_link']);

        //count task on open append and completed.
        /*$task = Task::all();
        $count['new'] = $task->where('status',0)->count();
        $count['opened'] = $task->where('status',1)->count();
        $count['pending'] = $task->where('status',2)->count();
        $count['completed'] = $task->where('status',3)->count();*/

        $responseData = Tools::setResponse('success','Task Listing Based on Assigned User Successful.', $user,'');
        $status = 200;

        return response()->json($responseData,$status);
    }

    public function getTaskByStatus(Request $request){
        if(!isset($request->user_id) || !isset($request->limit) || !isset($request->status)){
            $responseData = Tools::setResponse('fail','Missing Parameter', '','');
            $status = 422;
            return response()->json($responseData,$status);
        }

        //accessing get values
        $user_id = $request->user_id;
        $limit = $request->limit;
        $status = $request->status;

//        note: if client id is deleted all the data are not retrieved
        $task = TaskUser::select('t.id', 't.name', 't.status','t.file','t.deadline','t.description','c.name as client_name','c.number as client_number','u.full_name as author','t.created_at','t.deadline','t.priority')
            ->leftjoin('tasks as t','task_user.task_id','t.id')
            ->leftjoin('users as u','u.id','t.created_by')
            ->leftjoin('clients as c','c.id','t.client_id')
            ->where('user_id',$user_id)
            ->where('t.status',$status)
            ->paginate($limit);

        $responseData = Tools::setResponse('success','Fetch New Task based on user id successfull.', $task,'');
        $status = 200;

        return response()->json($responseData,$status);

    }

    public function countTaskByStatus(Request $request){
        if(!isset($request->user_id)){
            $responseData = Tools::setResponse('fail','Missing Parameter', '','');
            $status = 422;
            return response()->json($responseData,$status);
        }

        $user_id =$request->user_id;
        //fetching all task except completed one.
        $user = User::where('id',$user_id)->with('tasks')->first();

        //count task on new, open, append and completed.
        $count['new'] = $user->tasks->where('status',0)->count();
        $count['opened'] = $user->tasks->where('status',1)->count();
        $count['pending'] = $user->tasks->where('status',2)->count();
        $count['completed'] = $user->tasks->where('status',3)->count();

        $responseData = Tools::setResponse('success','Task Counting on User Id Completed', $count,'');
        $status = 200;

        return response()->json($responseData,$status);
    }


    public function notification($token, $title,$description = null)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;

        $notification = [
            'title' => ucfirst($title),
            'text' => $description,
//            'assigned_by' => 'yonzon',
            // 'sound' => true,
             'sound' => "notification.mp3",
        ];

        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key= AAAA8iYzy1M:APA91bFgJiofpoZ_zn9xCGNHP8d16EAQw-hMKCB0X1pkJbi1DYX7Jr8_6WrETFXob8BWfuCUluUBqQfiRySDNj3egAxP4PtsHnOS1fp4HPnSOAjGV2kz377BiY32VJLYxkvw1c7rVDtS',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }



        curl_close($ch);

        return true;
    }




}
