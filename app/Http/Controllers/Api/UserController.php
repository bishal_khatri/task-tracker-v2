<?php

namespace App\Http\Controllers\Api;

use App\Client;
use App\Helper\Tools;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function addClient(Request $request){
        $validator = Validator::make($request->all(), [
            'client_name' => 'required|max:100',
            'number' => 'required|numeric|min:6|unique:clients',
            'latitude' => 'numeric|between:0,999.99',
            'longitude' => 'numeric|between:0,999.99',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Something went wrong', '', '');
            $status = 400;
            return response()->json($responseData, $status);
        }
        $client = new Client();
        $client->name = $request->client_name;
        $client->number = $request->number;
        $client->latitude = $request->latitude;
        $client->longitude = $request->longitude;
        $client->created_at = date('Y-m-d H:i:s');
        $client->save();
        $responseData = Tools::setResponse('success','Client added Successfully', $client,'');
        $status = 200;
        return response()->json($responseData,$status);

    }
    public function listUser(){
        $user = User::all();
        $responseData = Tools::setResponse('success','User Listing Successfully', $user,'');
        $status = 200;
        return response()->json($responseData,$status);
    }

    public function logout(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;
            return response()->json($responseData, $status);
        }

        $user = User::find($request->user_id);
        if($user !=null){
            $user->firebase_token = "";
            $user->save();
            $responseData = Tools::setResponse('success','Logout Successfully', '','');
        }

        else{
            $responseData = Tools::setResponse('fail','Error while Logout ', '','');

        }

        return response()->json($responseData,200);
    }
}
