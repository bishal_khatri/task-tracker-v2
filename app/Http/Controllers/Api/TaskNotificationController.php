<?php

namespace App\Http\Controllers\Api;

use App\Helper\Tools;
use App\TaskNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskNotificationController extends Controller
{
    public function getTaskNotificationByUserId(){
        if(!isset($_GET['user_id'])){
            $responseData = Tools::setResponse('fail','Missing Parameter', '','');
            $status = 422;
            return response()->json($responseData,$status);
        }

        $user_id =$_GET['user_id'];
        $notification = TaskNotification::where('user_id',$user_id)->orderby('id','desc')->get();

        $responseData = Tools::setResponse('success','Notification Task Listing Based on Assigned User Successfull.', $notification,'');
        $status = 200;

        return response()->json($responseData,$status);
    }
}
