<?php

namespace App\Http\Controllers;

use App\Helper\Tools;
use App\Telephone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TelephoneController extends Controller
{
    public function index(){
        $data['telephone'] = Telephone::all();
//        dd($data);
        return view('pages.telephone.index',$data);
    }

    public function registerTelephone(){
        return view("pages.telephone.register_telephone");
    }

    public function postRegisterTelephone(Request $request){
        $this->validate($request,[
           'name' => 'required|max:50',
           'department' => 'required|max:50',
           'post' => 'required|max:50',
           'contact' => 'required|digits_between:5,12',
//           'ext_number' => 'required|max:5'

        ]);

        if(isset($request->ext_number)){
            $this->validate($request,[
                'ext_number' => 'required|digits_between:3,5',
            ]);

        }

        $telephone = new Telephone();
        $telephone->name = $request->name;
        $telephone->department = $request->department;

        $telephone->post = $request->post;
        $telephone->contact = $request->contact;

        $telephone->ext_number = $request->ext_number;
        $telephone->created_at = date('Y-m-d H:i:s');

        if(isset($request->staff_image)){
            $telephone->staff_image = $request->file('staff_image')->store('files/staff_image','public');

        }
        $telephone->save();



        \Session::flash('success', 'New Telephone Directory Registered Successfully');
        return redirect()->route('telephone.index');
    }

    //get edit form
    public function editTelephone($id){
        $data['telephone'] = Telephone::find($id);
        return view ('pages.telephone.edit_telepohone',$data);
    }

    //edit in model here
    public function postEditTelephone(Request $request){
        $this->validate($request,[
            'name' => 'required|max:50',
            'department' => 'required|max:50',
            'post' => 'required|max:50',
            'contact' => 'required|digits_between:5,12',
//           'ext_number' => 'required|max:5'

        ]);

        if(isset($request->ext_number)){
            $this->validate($request,[
                'ext_number' => 'required|digits_between:3,5',
            ]);

        }


        $telephone = Telephone::find($request->telephone_id);
        $telephone->name = $request->name;
        $telephone->department = $request->department;

        $telephone->post = $request->post;
        $telephone->contact = $request->contact;

        $telephone->ext_number = $request->ext_number;
        $telephone->created_at = date('Y-m-d H:i:s');

        if(isset($request->staff_image)){
            \Storage::delete('public/'.$telephone->staff_image);
            $telephone->staff_image = $request->file('staff_image')->store('files/staff_image','public');

        }
        $telephone->save();

        \Session::flash('success', 'Telephone Directory Updated Successfully');
        return redirect()->route('telephone.index');
    }

    //ajax delete request
    public function deleteTelephone(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;

            return response()->json($responseData,$status);
        }

        //deleting group permission also
        $telephone = Telephone::where('id',$request->id)->first();
        $telephone->delete();

//        \Session::flash('success','User Deleted Successfully');
        $responseData = Tools::setResponse('success', 'Telephone Deleted Successfully', '', '');
        return response($responseData,200);
    }
}
