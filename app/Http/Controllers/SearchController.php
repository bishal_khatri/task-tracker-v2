<?php

namespace App\Http\Controllers;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function getResult(Request $request)
    {
    	
    	$results_task= Task::search($request->search)->get();
    	$results_user= User::search($request->search)->get();

    	return view('pages.search.result')->with('results_task', $results_task)->with('results_user', $results_user);
    	
    }

    public function list(Request $request)
    {
    	$term = $request->term;
    	dd($term);
    	$item = Task::where('item','LIKE','%'.$term.'%')->get();

    	return $item;
    	
    }
}
