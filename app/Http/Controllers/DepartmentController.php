<?php

namespace App\Http\Controllers;

use Session;
use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
	public function getIndex()
	{
        $depart = Department::all();
		return view('pages.department.index')->with('depart', $depart);
	}

    public function create()
    {
    	return view('pages.department.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		]);

    	$depart = new Department;
    	$depart->name = $request ->name;
    	$depart->save();

        Session::flash('success', 'Department added successfully');

    	return redirect()->route('department.home');
    }

    public function edit($id)
    {
        $depart = Department::find($id);
        return view('pages.department.edit')->with('depart', $depart);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            ]);

        $depart = Department::find($id);
        $depart->name = $request->input('name');
        $depart->save();

        Session::flash('success', 'Department Updated successfully');

        return redirect()->route('department.home');

    }
}
