<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskRemark extends Model
{
    protected  $table = "remarks";
    public $timestamps = false;
}
