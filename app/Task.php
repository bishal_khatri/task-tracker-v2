<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(array('id', 'full_name'));
    }

    public function clients()
    {
        return $this->belongsTo('App\Client', 'client_id')->select(array('id', 'name','number'));
    }


    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}



