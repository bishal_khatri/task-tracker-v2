<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//representational state transfer

Route::group(['middleware' => 'api' ], function () {
    Route::post('/login',['uses' => 'Api\LoginController@login']);
    Route::post('/change/status',['uses' => 'Api\TaskController@changeStatus']);


    Route::group(['prefix' => 'task'],function(){
        //retrieve task by userid excluding status 3 (completed)
        Route::get('/',['uses' => 'Api\TaskController@getTaskByUserId']);

        Route::get('/client/list', ['uses' => 'Api\TaskController@ClientList']);

        Route::post('/store',['uses' => 'Api\TaskController@storeTask']);
        Route::post('/status',['uses' => 'Api\TaskController@getTaskByStatus']);

        Route::get('/count',['uses' => 'Api\TaskController@countTaskByStatus']);


        //retrieve notification message and name based on user id.
        Route::get('/notification',['uses' => 'Api\TaskNotificationController@getTaskNotificationByUserId']);
    });

    Route::group(['prefix' => 'telephone'],function(){
        //retrieve all telephone list
        Route::get('/list',['uses' => 'Api\TelephoneController@showAllTelephoneDirectory']);
    });

    Route::post('/client/add', ['uses' => 'Api\UserController@addClient']);//->middleware(['auth','admin']);
    Route::get('/user/list',['uses' => 'Api\UserController@listUser']);

    //clear token after logout
    Route::post('/logout','Api\UserController@logout');

});
