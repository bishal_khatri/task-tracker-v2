<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if(!defined('STATIC_DIR')) define("STATIC_DIR","public/");
if(!defined('DEFAULT_USER')) define('DEFAULT_USER','public/icon/default-user-1.png');

//redirecting to home
Route::get('/',['middleware' => 'auth', 'uses' => 'HomeController@index']);
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
/**
 * Staff management
 */
Route::group(['prefix' => 'staff','as'=>'staff.'], function () {
    Route::get('/',['uses' => 'Auth\RegisterController@show','as' => 'show']);
    Route::get('/edit/{id}', ['uses' => 'Auth\RegisterController@getEdit','as' => 'edit']);
    Route::post('/edit/{id}', ['uses' => 'Auth\RegisterController@update']);
    Route::get('/view/{id}', ['uses' => 'Auth\RegisterController@getView','as' => 'view']);
    Route::post('/delete', ['uses' => 'Auth\RegisterController@delete','as' => 'delete']);
    Route::get('/role', ['uses' => 'Auth\RegisterController@editRole','as' => 'role']);
    Route::post('/role', ['uses' => 'Auth\RegisterController@updateRole']);
});

Route::get('/profile/view/{id}', ['uses' => 'ProfileController@view','as' => 'profile.view']);
Route::get('/password/edit', ['uses' => 'ProfileController@editPassword','as' => 'password.edit']);
Route::post('password/edit/{id}', ['uses' => 'ProfileController@updatePassword','as' => 'password.update']);

/**
 *  Department
 */
Route::group(['prefix' => 'department','as'=>'department.'], function () {
    Route::get('/index', ['uses' => 'DepartmentController@getIndex','as' => 'home']);
    Route::get('/', ['uses' => 'DepartmentController@create','as' => 'create']);
    Route::post('/', ['uses' => 'DepartmentController@store','as' => 'create']);
    Route::get('/edit/{id}', ['uses' => 'DepartmentController@edit','as' => 'edit']);
    Route::post('/edit/{id}', ['uses' => 'DepartmentController@update']);
});

/**
 *  Task management
 */
Route::group(['prefix' => 'task','as'=>'task.'], function () {
    Route::get('/create', ['uses' => 'TaskController@create','as' => 'create'])->middleware(['auth','admin']);
    Route::post('/create', ['uses' => 'TaskController@store','as' => 'store'])->middleware(['auth','admin']);
    Route::get('/show', ['uses' => 'TaskController@show','as' => 'show'])->middleware(['auth']);
    Route::get('/completed', ['uses' => 'TaskController@showCompletedTask','as' => 'show_completed_task'])->middleware(['auth']);

    Route::get('/assign', ['uses' => 'TaskController@getAssign','as' => 'assign'])->middleware(['auth','admin']);
    Route::post('/assign', ['uses' => 'TaskController@assign','as' => 'assign'])->middleware(['auth','admin']);
    Route::get('/assign/id', ['uses' => 'TaskController@getAssignedUserById','as' => 'getUserByTaskId'])->middleware(['auth','admin']);


    Route::get('/view/{id}',['uses' => 'TaskController@viewTask','as' => 'view']);
    Route::get('/status/{id}', ['uses' => 'TaskController@status','as' => 'status']);
    Route::get('/edit/{id}', ['uses' => 'TaskController@edit','as' => 'edit'])->middleware(['auth','admin']);
    Route::post('/edit/{id}', ['uses' => 'TaskController@update'])->middleware(['auth','admin']);
    Route::post('/delete', ['uses' => 'TaskController@delete','as' => 'delete'])->middleware(['auth','admin']);

    //change task status plus add remarks.
    Route::post('/remarks/store', ['uses' => 'TaskController@addRemarks', 'as' => 'store_remarks'])->middleware(['auth']);

    //get task  from usre id
    Route::get('/user/task',['uses' => 'TaskController@getTaskByUserId', 'as' => 'user-task']);

    //client routes
    Route::get('/client/show',['uses' => 'TaskController@getAllClient', 'as' => 'client-retrieve'])->middleware(['auth','admin']);
    Route::post('/client/add', ['uses' => 'TaskController@storeClient','as' => 'client-store'])->middleware(['auth','admin']);
    Route::post('/client/delete', ['uses' => 'TaskController@deleteClient','as' => 'client-delete'])->middleware(['auth','admin']);

    Route::post('/client/retrieve/clientinfo',['uses' => 'TaskController@getClientById', 'as' => 'client-retrieve-id'])->middleware(['auth','admin']);
    Route::post('/client/edit', ['uses' => 'TaskController@editClient','as' => 'client-edit'])->middleware(['auth','admin']);



});


/**
 *  Search
 */
Route::post('/result', ['uses' => 'SearchController@getResult','as' => 'search.result']);

Route::get('/list', ['uses' => 'SearchController@list']);

/**
 * Comment
 */
Route::post('/comment/{taskId}', ['uses' => 'CommentController@store','as' => 'comment']);

Route::get('/comment/delete/{id}', ['uses' => 'CommentController@delete','as' => 'comment.delete']);

Route::get('/comment/update/{id}', ['uses' => 'CommentController@edit','as' => 'comment.update']);

Route::post('/comment/update/{id}', ['uses' => 'CommentController@update','as' => 'comment.update']);



/**
 * Telephone Directory
 */
Route::group(['prefix' => 'telephone','as' => 'telephone.'],function(){
   Route::get('/',['uses' => 'TelephoneController@index', 'as' => 'index']);
   Route::get('/register',['uses' => 'TelephoneController@registerTelephone', 'as' => 'register_telephone']);
   Route::post('/register',['uses' => 'TelephoneController@postRegisterTelephone', 'as' => 'register_telephone']);

   //edit telephone
    Route::get('/edit/{id}',['uses' => 'TelephoneController@editTelephone', 'as' => 'edit_telephone']);
    Route::post('/update',['uses' => 'TelephoneController@postEditTelephone', 'as' => 'update_telephone']);

    //delete telephone information
    Route::post('/delete',['uses' => 'TelephoneController@deleteTelephone', 'as' => 'delete_telephone']);


});

/**
 * Application Leave
 */
Route::group(['prefix' => 'leave','as' => 'leave.'],function(){
   Route::get('/',['uses' => 'LeaveController@index', 'as' => 'list']);
   Route::get('/application/fill',['uses' => 'LeaveController@registerLeave', 'as' => 'register_leave']);
   Route::post('/register',['uses' => 'LeaveController@postRegisterLeave', 'as' => 'store_leave']);

   //edit telephone
    Route::get('/edit/{id}',['uses' => 'LeaveController@editTelephone', 'as' => 'edit_telephone']);
    Route::post('/update',['uses' => 'LeaveController@postEditTelephone', 'as' => 'update_telephone']);

    //delete telephone information
    Route::post('/delete',['uses' => 'LeaveController@deleteTelephone', 'as' => 'delete_telephone']);


});