<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');

            $table->string('full_name');

            $table->string('contact')->nullable();

            $table->string('email')->nullable();

            $table->string('permanent_address')->nullable();

            $table->string('temporary_address')->nullable();

            $table->string('facebook_link')->nullable();

            $table->string('google_link')->nullable();

            $table->string('twitter_link')->nullable();

            $table->string('linkedin_link')->nullable();

            $table->date('join_date')->nullable();

            $table->string('department')->nullable();

            $table->string('profile_image')->nullable();

            $table->string('citizenship_image')->nullable();

            $table->boolean('user_type');

            $table->string('user_name')->unique();

            $table->string('password');

//            $table->timestamp('last_login_at')->nullable();

            $table->rememberToken();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
