<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'full_name' => 'Administrator',
            'user_name' => 'administrator',
            'user_type' => 1,
            'profile_image' => null,
            'password' => bcrypt('password'),
            'created_at' => date('Y-m-d H:i:s'),
            ]);
    }
}
